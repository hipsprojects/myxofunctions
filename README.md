# README #

### What is this repository for? ###

* MxBase Explorer Apache Thrift Server Functions
* Version 1.26

It is used for mining the secondary metabolomes of myxobacteria, using statistical evaluation of mass spectrometry data. 
The analysis is supported by functions of the Mxbase server component.

### How do I get set up? ###

* Summary of set up
You can use the functions either via MxBase Explorer or tools like KNIME  
In order to execute the functions, you need to create a thrift server client connection and a database connection 
Available functions only for mining the secondary metabolomes of myxobacteria: Metabolome Combiner, Matrix Filter, Threshold Filter
You can feed the data into the KNIME nodes to create Distance Matrix, Hierarchial Clustering and a Heatmap

Example usage for KNIME Java Variable Node:  
//Client Connection		 
TTransport transport = new TFramedTransport(new TSocket("localhost", 8181));
TCompactProtocol protocol = new TCompactProtocol(transport);
ThriftService.Client client = new ThriftService.Client(protocol); 
transport.open();
//Database Connection
client.mySQLConnect("localhost:8080", "myxobase", "username", "userpass"); 
//MxBase Function
client.matrixFilter(massRange, retTimeRange, mainTable, matrixTable, operationId, metaKeyList);
	
### Who do I talk to? ###

* Repo owner or admin
daniel.krug@helmholtz-hzi.de
* Other community or team contact
nisabozkurt@gmail.com
srikanth.duddela@gmail.com