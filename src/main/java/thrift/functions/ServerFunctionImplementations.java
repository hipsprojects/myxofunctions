/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

import java.io.IOException;
import java.io.InputStream;
import java.sql.Blob;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.xml.parsers.ParserConfigurationException;
import org.xml.sax.SAXException;
import thrift.properties.Properties;
import com.ggasoftware.indigo.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 *
 * @author Zhazira-pc
 */
public class ServerFunctionImplementations {

    /**
     * Tests the MySQL connection with the default Thrift_User.
     *
     * @param server
     * @param database
     * @param userName
     * @return
     */
    public String testMySQLConnection(String server,
            String database, String userName) {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            return null;
        }
        String result = "";
        Connection connection = null;
        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://" + server + "/" + database + "?useUnicode=true&characterEncoding=UTF-8",
                            Properties.THRIFT_USER,//.ZHAZ_USER,
                            Properties.THRIFT_USER_PASSWORD//.ZHAZ_USER_PASSWORD
                    );

        } catch (SQLException e) {
            result += e.getMessage().toString();
        }

        boolean check = checkUser(userName, connection);

        if (connection != null) {
            if (check) {

                result += "Success";
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ServerFunctionImplementations.class.getName()).log(Level.SEVERE, null, ex);
                }

            } else {
                connection = null;
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ServerFunctionImplementations.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else {
            result = "Failed to connect: " + result;
        }

        return result;
    }

    /**
     * Connects to the MySQL database server with the default Thrift_User.
     *
     * @param server server name
     * @param database database name
     * @param userName omit
     * @param userpw omit
     * @return
     */
    public Connection getMySQLConnection(String server,
            String database,
            String userName,
            String userpw) {

        System.out.println("GET");
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            return null;
        }

        Connection connection = null;

        try {
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://" + server + "/" + database + "?useUnicode=true&characterEncoding=UTF-8",
                            Properties.THRIFT_USER,//.ZHAZ_USER,
                            Properties.THRIFT_USER_PASSWORD//.ZHAZ_USER_PASSWORD
                    );

        } catch (SQLException e) {
            return connection;
        }
        boolean check = checkUser(userName, connection);

        if (connection != null) {
            if (check) {
                //System.out.println("Username exists in the database!");
                //System.out.println("You made it, take control your database now!");
            } else {
                //System.out.println("Username does not exist in the database!");
                connection = null;
                try {
                    connection.close();
                } catch (SQLException ex) {
                    Logger.getLogger(ServerFunctionImplementations.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        } else {
            //System.out.println("Failed to make connection!");
        }
        return connection;
    }

    /**
     * Checks if the user exists in the Database.
     *
     * @param userName
     * @param connection
     * @return
     */
    public boolean checkUser(String userName, Connection connection) {

        boolean check = false;

        List<String> userList = new ArrayList<String>();
        try {
            Statement stmt = connection.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            ResultSet userResult = stmt.executeQuery("select UserName, Usertag from UserList;");

            while (userResult.next()) {
                String combination = "";
                String name = userResult.getString("UserName");
                combination += name;
                combination += "_UT_";
                String tag = userResult.getString("Usertag");
                combination += tag;
                userList.add(combination);
            }

            if (userList.contains(userName)) {
                check = true;
            } else {
                check = false;
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServerFunctionImplementations.class.getName()).
                    log(Level.SEVERE,
                            null,
                            ex);
        }
        return check;
    }

    /**
     * Returns obtained TA Screening export results for each compound key in a
     * given c_ids list.
     *
     * @param headerLine include header line
     * @param withoutAnalData include compounds without Analytical data
     * @param retentionTime include compounds without Retention time
     * @param pseuFormulae generate neutral pseudo-formulae
     * @param analDetailsToNames append analytical details to names
     * @param polarity the polarity value
     * @param analSystem the Analytical System value
     * @param cIDList the list of selected compound keys
     * @param connection omit
     * @return list of all export result lines
     */
    public List<String> BruckerTAScrrening(boolean headerLine,
            boolean withoutAnalData,
            boolean retentionTime,
            boolean pseuFormulae,
            boolean analDetailsToNames,
            String polarity,
            String analSystem,
            List<Integer> cIDList,
            Connection connection) {
        List<String> resultList = new ArrayList<String>();
        String str = "";

        if (headerLine) {
            //str = "m/z,rt,formula,name,rn,id2,id3,relativeRetentiontime,area,indivSigma,indivMassError,Qual1,Qual2,Qual3,QualMin1,QualMax1,QualMin2,QualMax2,QualMin3,QualMax3,TLISKey";

            str = "m/z,rt,formula,name,rn,id2,id3,relativeRetentiontime,area,indivSigma,indivMassError,Qual1,Qual2,Qual3,QualMin1,QualMax1,QualMin2,QualMax2,QualMin3,QualMax3";
            //str += System.getProperty("line.separator");
            resultList.add(str);
        }

        try {
            for (int c_id : cIDList) {
                Statement stmt = connection.createStatement(
                        ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                ResultSet rs1 = stmt.executeQuery(
                        "select  t.c_IonMass as 'Ion m/z',t.c_RetentionTime as 'Ret.', "
                        + " k.c_IonCharge as 'Charge',t.c_IonFormula as 'Formula',t.c_ID,t.c_AnalyticalSystemID,"
                        + " t.c_type1key as 'LCMS key',t.c_IonType ,k.c_IonTypeExplained as 'Iontype',"
                        + " t.c_MinArea as 'Min.area', k.c_IonTypeRule,t.c_IndivError as 'err', t.c_IndivSigma as 'sig',"
                        + " t.c_QualIon1 as 'qi1', t.c_QualIon2 as 'qi2', t.c_QualIon3 as 'qi3',"
                        + " t.c_QualMin1 as 'qmin1', t.c_QualMin2 as 'qmin2', t.c_QualMin3 as 'qmin3',"
                        + " t.c_QualMax1 as 'qmax1', t.c_QualMax2 as 'qmax2', t.c_QualMax3 as 'qmax3',"
                        + " t.c_IonCharge as 'ionCharge', t.c_IonFormula as 'IonFormula', c.c_name as 'c_Name', "
                        + " c.c_formula from C_Analytical_Type1 t "
                        + " left join C_Analytical_Type1_IonTypes k on k.c_iontype = t.c_iontype"
                        + " left join C_Compound c on c.c_id = t.c_ID "
                        + " where t.c_id= '" + c_id + "' and "
                        + " t.c_analyticalsystemid = '" + analSystem + "';");
                int count = 0;
                while (rs1.next()) {
                    count++;
                    str = "";
                    if (rs1.getString("c_Name") == null) {
                        continue;
                    }
                    int ionCharge = rs1.getInt("ionCharge");
                    String ionChargewithSign = "";

                    if (ionCharge >= 0) {
                        if (polarity.equals("Negative")) {
                            continue;
                        }
                        ionChargewithSign = String.valueOf(rs1.getInt(
                                "Charge")) + "+";
                    } else {
                        if (polarity.equals("Positive")) {
                            continue;
                        }
                        ionChargewithSign = String.valueOf(Math.abs(rs1.getInt(
                                "Charge"))) + "-";
                    }

                    double ion = rs1.getDouble("Ion m/z");
                    String ionstr = "";
                    double retTime = rs1.getDouble("Ret.");
                    if (rs1.wasNull()) {
                        if (!retentionTime) {
                            continue;
                        }
                        str += ionstr + ",,";
                    }
                    String retTimestr = "";
                    if (ion == (int) ion) {
                        ionstr = String.format(Locale.US,
                                "%d",
                                (int) ion);
                    } else {
                        ionstr = String.format(Locale.US,
                                "%s",
                                ion);
                    }

                    if (retTime == (int) retTime) {
                        if ((retTime == (double) 0) && !retentionTime && !withoutAnalData) {
                            continue;
                        }
                        retTimestr = String.format(Locale.US,
                                "%d",
                                (int) retTime);
                    } else {
                        retTimestr = String.format(Locale.US,
                                "%s",
                                retTime);
                    }
                    str
                            += ionstr + ","
                            + retTimestr + ",";

                    if (pseuFormulae) {
                        String str137 = parseIonFormula(rs1.getString("Formula"),
                                polarity);
                        str += String.format(Locale.US,
                                "%s",
                                str137) + ",";
                    } else {
                        str += String.format(Locale.US,
                                "%s",
                                rs1.getString("IonFormula"))
                                + "^" + ionChargewithSign + ",";
                    }
                    if (analDetailsToNames) {
                        str += String.format(Locale.US,
                                "%s",
                                rs1.getString("c_Name")) + "  #  ";
                    } else {
                        str += String.format(Locale.US,
                                "%s",
                                rs1.getString("c_Name")) + ",";
                    }
                    String errStr = "";
                    double err = rs1.getDouble("err");
                    if (!(err == (double) 0) && !rs1.wasNull()) {
                        if (err == (int) err) {
                            errStr = String.format(Locale.US,
                                    "%d",
                                    (int) err);
                        } else {
                            errStr = String.format(Locale.US,
                                    "%s",
                                    err);
                        }
                    }

                    String sigStr = "";
                    double sig = rs1.getDouble("sig");
                    if (!(sig == (double) 0) && !rs1.wasNull()) {
                        if (sig == (int) sig) {
                            sigStr = String.format(Locale.US,
                                    "%d",
                                    (int) sig);
                        } else {
                            sigStr = String.format(Locale.US,
                                    "%s",
                                    sig);
                        }
                    }

                    String qi1Str = "";
                    double qi1 = rs1.getDouble("qi1");
                    if (!(qi1 == (double) 0) && !rs1.wasNull()) {
                        if (qi1 == (int) qi1) {
                            qi1Str = String.format(Locale.US,
                                    "%d",
                                    (int) qi1);
                        } else {
                            qi1Str = String.format(Locale.US,
                                    "%s",
                                    qi1);
                        }
                    }

                    String qi2Str = "";
                    double qi2 = rs1.getDouble("qi2");
                    if (!(qi2 == (double) 0) && !rs1.wasNull()) {
                        if (qi2 == (int) qi2) {
                            qi2Str = String.format(Locale.US,
                                    "%d",
                                    (int) qi2);
                        } else {
                            qi2Str = String.format(Locale.US,
                                    "%s",
                                    qi2);
                        }
                    }

                    String qi3Str = "";
                    double qi3 = rs1.getDouble("qi3");
                    if (!(qi3 == (double) 0) && !rs1.wasNull()) {
                        if (qi3 == (int) qi3) {
                            qi3Str = String.format(Locale.US,
                                    "%d",
                                    (int) qi3);
                        } else {
                            qi3Str = String.format(Locale.US,
                                    "%s",
                                    qi3);
                        }
                    }
                    String qmin1Str = "";
                    double qmin1 = rs1.getDouble("qmin1");
                    if (!(qmin1 == (double) 0) && !rs1.wasNull()) {
                        if (qmin1 == (int) qmin1) {
                            qmin1Str = String.format(Locale.US,
                                    "%d",
                                    (int) qmin1);
                        } else {
                            qmin1Str = String.format(Locale.US,
                                    "%s",
                                    qmin1);
                        }
                    }
                    String qmin2Str = "";
                    double qmin2 = rs1.getDouble("qmin2");
                    if (!(qmin2 == (double) 0) && !rs1.wasNull()) {
                        if (qmin2 == (int) qmin2) {
                            qmin2Str = String.format(Locale.US,
                                    "%d",
                                    (int) qmin2);
                        } else {
                            qmin2Str = String.format(Locale.US,
                                    "%s",
                                    qmin2);
                        }
                    }
                    String qmin3Str = "";
                    double qmin3 = rs1.getDouble("qmin3");
                    if (!(qmin3 == (double) 0) && !rs1.wasNull()) {
                        if (qmin3 == (int) qmin3) {
                            qmin3Str = String.format(Locale.US,
                                    "%d",
                                    (int) qmin3);
                        } else {
                            qmin3Str = String.format(Locale.US,
                                    "%s",
                                    qmin3);
                        }
                    }
                    String qmax1Str = "";
                    double qmax1 = rs1.getDouble("qmax1");
                    if (!(qmax1 == (double) 0) && !rs1.wasNull()) {
                        if (qmax1 == (int) qmax1) {
                            qmax1Str = String.format(Locale.US,
                                    "%d",
                                    (int) qmax1);
                        } else {
                            qmax1Str = String.format(Locale.US,
                                    "%s",
                                    qmax1);
                        }
                    }
                    String qmax2Str = "";
                    double qmax2 = rs1.getDouble("qmax2");
                    if (!(qmax2 == (double) 0) && !rs1.wasNull()) {
                        if (qmax2 == (int) qmax2) {
                            qmax2Str = String.format(Locale.US,
                                    "%d",
                                    (int) qmax2);
                        } else {
                            qmax2Str = String.format(Locale.US,
                                    "%s",
                                    qmax2);
                        }
                    }
                    String qmax3Str = "";
                    double qmax3 = rs1.getDouble("qmax3");
                    if (!(qmax3 == (double) 0) && !rs1.wasNull()) {
                        if (qmax3 == (int) qmax3) {
                            qmax3Str = String.format(Locale.US,
                                    "%d",
                                    (int) qmax3);
                        } else {
                            qmax3Str = String.format(Locale.US,
                                    "%s",
                                    qmax3);
                        }
                    }
                    if (analDetailsToNames) {
                        str += String.valueOf(rs1.getInt(
                                "c_AnalyticalSystemID")) + ":" + rs1.
                                getString(
                                        "LCMS key") + ":" + String.valueOf(
                                        rs1.
                                        getInt("c_IonType")) + ":" + rs1.
                                getString("Iontype") + "," + String.valueOf(
                                        rs1.
                                        getInt("c_ID")) + "," + String.
                                valueOf(
                                        rs1.getInt("c_AnalyticalSystemID")) + ":" + rs1.
                                getString("LCMS key") + "," + String.
                                valueOf(
                                        rs1.getInt("c_IonType")) + ":" + rs1.
                                getString("Iontype") + ",," + String.
                                valueOf(
                                        rs1.getString("Min.area")) + "," + errStr + "," + sigStr + "," + qi1Str + "," + qi2Str + "," + qi3Str + "," + qmin1Str + "," + qmax1Str + "," + qmin2Str + "," + qmax2Str + "," + qmin3Str + "," + qmax3Str;
                    } else {
                        str
                                += String.valueOf(rs1.getInt("c_ID")) + "," + String.
                                valueOf(rs1.getInt("c_AnalyticalSystemID")) + ":" + rs1.
                                getString("LCMS key") + "," + String.
                                valueOf(rs1.getInt(
                                                "c_IonType")) + ":" + rs1.
                                getString("Iontype") + ",," + String.
                                valueOf(rs1.getInt("Min.area")) + "," + errStr + "," + sigStr + "," + qi1Str + "," + qi2Str + "," + qi3Str + "," + qmin1Str + "," + qmax1Str + "," + qmin2Str + "," + qmax2Str + "," + qmin3Str + "," + qmax3Str;
                    }
                    //str += System.getProperty("line.separator");
                    resultList.add(str);
                }

                if (withoutAnalData && count == 0) {
                    str = "";
                    String dataStr = "";
                    Pattern p = Pattern.compile("(([a-zA-Z]{1,2})(\\d+))");
                    rs1 = stmt.executeQuery(
                            "select c_formula,c_name from C_Compound where c_id = " + c_id + "");
                    String formula = "";
                    while (rs1.next()) {
                        formula = rs1.getString("c_formula");
                        Matcher m = p.matcher(formula);
                        while (m.find()) {
                            String mod = m.group(3).
                                    toString();
                            int num = Integer.valueOf(mod);
                            if (m.group(2).
                                    toString().
                                    equals("H") || m.group(2).
                                    toString().
                                    equals("h")) {
                                if (polarity.equals("Negative")) {
                                    num = num - 1;
                                } else {
                                    num = num + 1;
                                }
                                dataStr += m.group(2).
                                        toString() + num;
                            } else {
                                dataStr += m.group(2).
                                        toString() + num;
                            }
                        }
                        if (pseuFormulae) {
                            str += ",,"
                                    + String.format(Locale.US,
                                            "%s",
                                            formula) + ",";
                        } else {
                            if (polarity.equals("Negative")) {
                                str += ",," + dataStr + "^-1,";
                            } else {
                                str += ",," + dataStr + "^1+,";
                            }

                        }
                        String analDetails = "";
                        String polarityDetails1 = "";
                        String polarityDetails2 = "";
                        if (polarity.equals("Negative")) {
                            polarityDetails1 = ":LCMS0:1:[M-H]-";
                            polarityDetails2 = ":LCMS0,1:[M-H]-";
                        } else {
                            polarityDetails1 = ":LCMS0:1:[M+H]+";
                            polarityDetails2 = ":LCMS0,1:[M+H]+";
                        }
                        if (analDetailsToNames) {
                            analDetails = " # " + analSystem + polarityDetails1;
                        }
                        str += rs1.getString(
                                "c_name") + analDetails + ","
                                + String.valueOf(c_id) + "," + analSystem + polarityDetails2 + "," + "," + ",,,,,";

                        //str += System.getProperty("line.separator");
                        resultList.add(str);
                    }
                }
            }
        } catch (SQLException ex) {
            Logger.getLogger(ServerFunctionImplementations.class.getName()).
                    log(Level.SEVERE,
                            null,
                            ex);
        }

        return resultList;
    }

    private String parseIonFormula(String IonFormula,
            String polarity) {
        char[] charArray = IonFormula.toCharArray();
        String str15 = "";
        String str136 = "";
        String str137 = "";
        for (char c : charArray) {
            if (Character.isLetter(c) && Character.
                    isUpperCase(c)) {
                if (str15.equals("H")) {
                    int w = 0;
                    if (polarity.equals("Positive") || polarity.
                            equals("Both")) {
                        w = Integer.valueOf(str136) - 1;
                    } else if (polarity.equals("Negative")) {
                        w = Integer.valueOf(str136) + 1;
                    }
                    str137 += String.valueOf(w);
                }
                str15 = "";
                str15 += c;
                str137 += c;
            } else if (Character.isLetter(c) && Character.
                    isLowerCase(c)) {
                str137 += c;
            } else if (Character.isDigit(c)) {
                if (str15.equals("H")) {
                    str136 += c;
                } else {
                    str137 += c;
                }
            }
        }
        return str137;
    }

    /**
     *
     * @param SCRNList
     * @param TAScore
     * @param area
     * @param intensity
     * @param massDev
     * @param mSigma
     * @param connection
     * @return
     */
    public String taSreeningXMLParser(List<String> SCRNList, String TAScore, String area, String intensity, String massDev, String mSigma, Connection connection) {
        //cmp.init();
        Compound cmp = new Compound();
        ParseTAScreeningXML xmlparser = new ParseTAScreeningXML();
        cmp.setCheckTAScore(TAScore);
        cmp.setCheckArea(area);
        cmp.setCheckIntensity(intensity);
        cmp.setCheckMassDev(massDev);
        cmp.setCheckmSigma(mSigma);
        cmp.setFlag("0");

        if (!TAScore.equals(Properties.CHECK_DISABLED)) {
            cmp.setIsTAscore(true);
        }
        if (!area.equals(Properties.CHECK_DISABLED)) {
            cmp.setIsArea(true);
        }
        if (!intensity.equals(Properties.CHECK_DISABLED)) {
            cmp.setIsIntensity(true);
        }
        if (!massDev.equals(Properties.CHECK_DISABLED)) {
            cmp.setIsMassDev(true);
        }
        if (!mSigma.equals(Properties.CHECK_DISABLED)) {
            cmp.setIsMSigma(true);
        }
        try {
            cmp.setCount(0);

            for (String SCRNKey : SCRNList) {

                String[] parts = SCRNKey.split("(?<=\\D)(?=\\d)|(?<=\\d)(?=\\D)");
                cmp.setSCRN_id(SCRNKey);
                if (!parts[1].equals(" ")) {
                    cmp.setS_key(parts[1]);
                }

                Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet rs = stmt.executeQuery(
                        "select t.s_MXID, f.s_file from S_Metabolites_TAFiles f, S_Metabolites_TAScreening t where t.SCRN_ID = '" + SCRNKey + "' and t.SCRN_ID = f.SCRN_ID");

                while (rs.next()) {
                    cmp.setS_MxID(String.valueOf(rs.getInt("s_MXID")));

                    Blob File = (Blob) rs.getBlob("s_file");
                    InputStream binaryStream = File.getBinaryStream(1, File.length());
                    try {
                        xmlparser.TAScreeningXMLParser(binaryStream, cmp, connection);
                    } catch (ParserConfigurationException ex) {
                        cmp.exceptionMessageLogs += "ServerFunctionImplementations.taSreeningXMLParser(ParserConfigurationException): {" + ex.getMessage() + "};";
                    } catch (SAXException ex) {
                        cmp.exceptionMessageLogs += "ServerFunctionImplementations.taSreeningXMLParser(SAXException): {" + ex.getMessage() + "};";
                    } catch (IOException ex) {
                        cmp.exceptionMessageLogs += "ServerFunctionImplementations.taSreeningXMLParser(IOException): {" + ex.getMessage() + "};";
                    }
                }
            }
        } catch (SQLException ex) {
            cmp.exceptionMessageLogs += "ServerFunctionImplementations.taSreeningXMLParser(SQLException): {" + ex.getMessage() + "};";
        } catch (Exception ex) {
            cmp.exceptionMessageLogs += " TESTT ServerFunctionImplementations.taSreeningXMLParser(Exception): {" + ex.getMessage() + "};";
            ex.printStackTrace();
            ex.getMessage();
        }
        if (cmp.getExceptionMessageLogs().trim().equals("")) {
            return "Done";
        } else {
            return cmp.getExceptionMessageLogs().trim();
        }

    }

    /**
     *
     * @param ionFomrula
     * @param ionCharge
     * @return
     */
    public List<String> ipc(String ionFomrula, String ionCharge) {
        List<String> massAndIntenList = new ArrayList<String>();
        ParseTAScreeningXML xmlparser = new ParseTAScreeningXML();
        try {
            massAndIntenList = xmlparser.getMassesAndIntensities(ionFomrula, ionCharge);
        } catch (Exception ex) {
            Logger.getLogger(ServerFunctionImplementations.class.getName()).log(Level.SEVERE, null, ex);
        }
        return massAndIntenList;
    }

    /**
     *
     * @param METAList
     * @param intensity
     * @param area
     * @param peaksNo
     * @param retTime
     * @param recalculateCharge
     * @param connection
     * @return
     */
    public String metabolomeMiningXMLParser(List<String> METAList, String intensity, String area, String peaksNo, String retTime, boolean recalculateCharge, Connection connection) {

        for (String METAKey : METAList) {
            try {
                ParseMetabolomeMiningXML metMiningXmlParser = new ParseMetabolomeMiningXML();
                if (!intensity.equals(Properties.CHECK_DISABLED)) {
                    metMiningXmlParser.checkIntensity = Double.parseDouble(intensity.replace(",", "."));
                }
                if (!area.equals(Properties.CHECK_DISABLED)) {
                    metMiningXmlParser.checkArea = Double.parseDouble(area.replace(",", "."));
                }
                if (!peaksNo.equals(Properties.CHECK_DISABLED)) {
                    metMiningXmlParser.checkPeakNo = Integer.valueOf(peaksNo);
                }
                if (!retTime.equals(Properties.CHECK_DISABLED)) {
                    String[] parts = retTime.split(";");
                    metMiningXmlParser.checkRetTimeFrom = Double.parseDouble(parts[0].replace(",", "."));
                    metMiningXmlParser.checkRetTimeTo = Double.parseDouble(parts[1].replace(",", "."));
                }

                Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);

                ResultSet rs = stmt.executeQuery(
                        "select b.META_blob,d.Analyticalsystem from S_Metabolome_blobs b,S_Metabolome_data d where b.META_ID = '" + METAKey + "' and b.META_ID = d.META_key");
                while (rs.next()) {
                    Blob File = (Blob) rs.getBlob("META_blob");
                    InputStream binaryStream = File.getBinaryStream(1, File.length());
                    metMiningXmlParser.METAXMLParser(binaryStream, METAKey, String.valueOf(rs.getInt("Analyticalsystem")), recalculateCharge, connection);
                }
            } catch (SQLException ex) {
            } catch (Exception ex) {
            }
        }
        return "";
    }

    /**
     *
     * @param QRY_IDList
     * @param connection
     */
    public void metabolomeMiningQuery(List<String> QRY_IDList, Connection connection) {

        for (String qryId : QRY_IDList) {
            try {
                ParseExperimentParametersAndQueryResults parser = new ParseExperimentParametersAndQueryResults();
                Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                        ResultSet.CONCUR_READ_ONLY);
                ResultSet rs = stmt.executeQuery("select QRY_parameters,QRY_ID from QRY_Experiments where QRY_Key = " + qryId + "");
                String parameters = "";
                String QRY_ID = "";
                while (rs.next()) {
                    parameters = rs.getString("QRY_parameters");
                    QRY_ID = rs.getString("QRY_ID");
                }
                if (!parameters.equals("")) {
                    parser.parseExperimentParameters(qryId, QRY_ID, parameters, connection);
                    Statement st = (Statement) connection.createStatement();
                    st.executeUpdate("update QRY_Experiments set QRY_status = '1' where QRY_Key = " + qryId + "");
                }
            } catch (SQLException e) {
            } catch (Exception e) {
            }
        }
    }

    /**
     *
     * @param polarity
     * @param ion_list
     * @param c_ids
     * @param connection
     * @return
     */
    public List<String> generateIonList(String polarity, List<String> ion_list, List<Integer> c_ids, Connection connection) {

        List<String> resultList = new ArrayList<String>();
        String str = "";
        String headerLine = "CompoundID\tCompoundName\tCompoundFormula\tCompoundFamily\tCategoryName\tOriginName\tNeutralMass\tIonType\tIonFormula\tIonizedFormula\tIonizedMass";
        resultList.add(headerLine);

        for (int CompoundKey : c_ids) {
            for (String IonType : ion_list) {

                try {
                    str = "";
                    Statement stmt1;
                    stmt1 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);

                    ResultSet rs1 = stmt1.executeQuery("select c_id, c_name, c_formula, f_id, cat_name, o_name from C_Compound where c_id = '" + CompoundKey + "'");
                    String c_formula = "";
                    while (rs1.next()) {
                        String c_id = String.valueOf(rs1.getString("c_id"));
                        str += c_id + "\t";
                        String c_name = rs1.getString("c_name");
                        str += c_name + "\t";
                        c_formula = rs1.getString("c_formula");
                        str += c_formula + "\t";
                        String f_id = rs1.getString("f_id");
                        str += f_id + "\t";
                        String cat_name = rs1.getString("cat_name");
                        str += cat_name + "\t";
                        String o_name = rs1.getString("o_name");
                        str += o_name + "\t";
                    }

                    double monoMassCmp = 0.0;
                    try {
                        monoMassCmp = monoMass(c_formula);
                    } catch (IndigoException ex) {
                    }

                    double factor = 1e5; // = 1 * 10^5 = 100000.
                    double monoMassCmpRound = Math.round(monoMassCmp * factor) / factor;

                    str += monoMassCmpRound + "\t";
                    Statement stmt2 = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE,
                            ResultSet.CONCUR_READ_ONLY);
                    ResultSet rs2 = stmt2.executeQuery("select c_IonType, c_IonTypeExplained, c_IonTypeRule, c_IonMultimeric, c_IonCharge from C_Analytical_Type1_IonTypes where c_IonTypeExplained = '" + IonType + "'");

                    String c_IonTypeRule = "";
                    String c_IonMultimeric = "";
                    String c_IonCharge = "";
                    while (rs2.next()) {
                        String c_IonType = rs2.getString("c_IonType");
                        str += c_IonType + "\t";
                        String c_IonTypeExplained = rs2.getString("c_IonTypeExplained");
                        str += c_IonTypeExplained + "\t";
                        c_IonTypeRule = rs2.getString("c_IonTypeRule");
                        c_IonMultimeric = rs2.getString("c_IonMultimeric");
                        c_IonCharge = rs2.getString("c_IonCharge");
                    }

                  
                    String ionMulti = c_IonMultimeric;
                    String ChemOne = c_formula;

                    String pattern = "(([a-zA-Z]{1,2})(\\d+))";
                    Pattern p1 = Pattern.compile(pattern);
                    Matcher m = p1.matcher(ChemOne);

                    String cmpCount = "";
                    String cmpAtom = "";
                    String newCmpFormula = "";
                    int multi;
                    while (m.find()) {

                        cmpCount = m.group(3).toString();
                        cmpAtom = m.group(2).toString();

                        multi = Integer.valueOf(cmpCount) * Integer.valueOf(ionMulti);
                        newCmpFormula += cmpAtom;
                        newCmpFormula += multi;

                    }

                    String matchedFormula = matchFormulas(newCmpFormula, c_IonTypeRule);

                    String grossFormula = grossFormula(matchedFormula);
                    str += grossFormula + "\t";

                    Double cmpIonMonoMass = monoMass(matchedFormula);
                    Double newMass = cmpIonMonoMass;
                    String ion = c_IonCharge;
                    Double ionCharge = 0.0, electronCal = 0.0, k1 = 0.0, a1 = 0.0;

                    ionCharge = Double.valueOf(ion);
                    electronCal = ionCharge * 0.0005486;

                    k1 = Math.abs(electronCal);
                    a1 = Math.abs(ionCharge);

                    if (electronCal < 0) {
                        newMass = ((cmpIonMonoMass + k1) / a1);
                    } else {
                        newMass = ((cmpIonMonoMass - k1) / a1);
                    }

                    double newMassRound = Math.round(newMass * factor) / factor;
                    str += newMassRound;

                    resultList.add(str);
                } catch (SQLException ex) {
                    Logger.getLogger(ServerFunctionImplementations.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return resultList;
    }

    private String matchFormulas(String newCmpFormula, String c_IonTypeRule) {

        String pattern1 = "([A-Za-z]+[0-9]+)";
        Pattern p1 = Pattern.compile(pattern1);
        String pattern2 = "([\\+\\-][A-Za-z]+[0-9]+)";
        Pattern p2 = Pattern.compile(pattern2);
        Map<String, Integer> formulaMatched = new HashMap<String, Integer>();

        Matcher mCmp = p1.matcher(newCmpFormula);
        while (mCmp.find()) {
            int p = 0;
            while (Character.isLetter(mCmp.group(0).toString().charAt(p))) {
                p++;
            }
            formulaMatched.put(mCmp.group(0).toString().substring(0, p), Integer.parseInt(mCmp.group(0).toString().substring(p)));
        }

        Matcher mIon = p2.matcher(c_IonTypeRule);

        while (mIon.find()) {

            char action = mIon.group(0).toString().charAt(0);
            int p = 1;

            while (Character.isLetter(mIon.group(0).toString().charAt(p))) {
                p++;
            }
            String chem = mIon.group(0).toString().substring(1, p);
            int amount = Integer.parseInt(mIon.group(0).toString().substring(p));
            amount = action == '+' ? amount : amount * -1;
            if (formulaMatched.containsKey(chem)) {
                formulaMatched.put(chem, formulaMatched.get(chem) + amount);

            } else {
                if (action != '-') {
                    formulaMatched.put(chem, amount);
                }
            }

        }
        StringBuilder sb = new StringBuilder();
        for (String key : formulaMatched.keySet()) {
            if (!formulaMatched.get(key).toString().equals("0")) {
                sb.append(key);
                sb.append(formulaMatched.get(key));
            }
        }
        return sb.toString();
    }

    private double monoMass(String formula) {

        double monoMass = 0.0;

        Indigo indigo = new Indigo();
        indigo.setOption("ignore-stereochemistry-errors", "true");
        IndigoObject newmol = indigo.createMolecule();
        Map<String, Integer> calculateMass = parseFormula(formula);

        for (String item : calculateMass.keySet()) {
            for (int i = 0; i < calculateMass.get(item); i++) {
                IndigoObject atom = newmol.addAtom(item);
                if (!item.equals("H")) {
                    atom.setImplicitHCount(0);
                }
            }
        }
        monoMass = newmol.monoisotopicMass();
        return monoMass;
    }

    private String grossFormula(String formula) {

        String grossFormula = "";

        Indigo indigo = new Indigo();
        indigo.setOption("ignore-stereochemistry-errors", "true");
        IndigoObject newmol = indigo.createMolecule();

        Map<String, Integer> calculateMass = parseFormula(formula);

        for (String item : calculateMass.keySet()) {
            for (int i = 0; i < calculateMass.get(item); i++) {
                IndigoObject atom = newmol.addAtom(item);
                if (!item.equals("H")) {
                    atom.setImplicitHCount(0);
                }
            }
        }
        grossFormula = newmol.grossFormula();
        return grossFormula;
    }

    //Naive Method to Merge 2 Formulas
    private IndigoObject mergeFrmls(String c_formula, String c_IonTypeRule) {
        Indigo indigo = new Indigo();
        indigo.setOption("ignore-stereochemistry-errors", "true");
        IndigoObject newmol = indigo.createMolecule();

        Map<String, Integer> formulaMapCompound = parseFormula(c_formula);
        for (String item : formulaMapCompound.keySet()) {
            for (int i = 0; i < formulaMapCompound.get(item); i++) {
                IndigoObject atom = newmol.addAtom(item);
                if (!item.equals("H")) {
                    atom.setImplicitHCount(0);
                }
            }
        }

        Map<String, Integer> formulaMapIon = parseFormula(c_IonTypeRule);
        for (String item : formulaMapIon.keySet()) {
            for (int i = 0; i < formulaMapIon.get(item); i++) {
                IndigoObject atom = newmol.addAtom(item);
                if (!item.equals("H")) {
                    atom.setImplicitHCount(0);
                }
            }
        }
        return newmol;
    }

    private Map<String, Integer> parseFormula(String formula) {

        Map<String, Integer> formulaMap = new HashMap<String, Integer>();

        Pattern p = Pattern.compile("[A-Za-z]+|[0-9]+");
        Matcher m = p.matcher(formula);
        String atom = "";
        while (m.find()) {
            if (m.group(0).matches("[a-zA-Z]+") == true) {
                atom = m.group(0);
            }
            if (m.group(0).matches("[0-9]+") == true) {
                formulaMap.put(atom, Integer.valueOf(m.group(0)));
            }
        }
        return formulaMap;
    }

    /**
     *
     * @param mainUsername
     * @param massRange
     * @param retTimeRange
     * @param mainTable
     * @param matrixTable
     * @param operationId
     * @param metaKeyList
     * @param connection
     */
    public void matrix(String mainUsername, double massRange, double retTimeRange, String mainTable, String matrixTable, int operationId, String metaKeyList, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String xmlMatrix = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><operation name=\"Filter\"><filtertype>MatrixFilter</filtertype><massrange>" + massRange + "</massrange><rettimerange>" + retTimeRange + "</rettimerange><firsttable>" + mainTable + "</firsttable><secondtable>" + matrixTable + "</secondtable><operationid>" + operationId + "</operationid><metaKeyList>" + metaKeyList + "</metaKeyList></operation>";

        String createQuery = "INSERT INTO S_Metabolite_Operations VALUES(Null, '" + xmlMatrix + "', '" + mainUsername + "', '" + dateFormat.format(date) + "' )";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(createQuery);
        } catch (SQLException ex) {
        }

        int nextOpId = 0;
        String selectQuery = "SELECT MAX(Operation_Key) FROM S_Metabolite_Operations";

        try {
            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = st.executeQuery(selectQuery);

            while (result.next()) {

                nextOpId = result.getInt("MAX(Operation_Key)");

            }

        } catch (SQLException ex) {
        }

        if (nextOpId == 0) {
            nextOpId = 1;
        }

        MatrixFilter matrixFilter = new MatrixFilter();

        matrixFilter.filter(mainUsername, massRange, retTimeRange, nextOpId, mainTable, matrixTable, operationId, metaKeyList, connection);

    }

    /**
     *
     * @param mainUsername
     * @param mzMin
     * @param mzMax
     * @param retTimeMin
     * @param retTimeMax
     * @param intenMin
     * @param intenMax
     * @param sn
     * @param peakNr
     * @param mArea
     * @param tableName
     * @param minRKey
     * @param maxRKey
     * @param operationId
     * @param connection
     */
    public void threshold(String mainUsername, double mzMin, double mzMax, double retTimeMin, double retTimeMax, double intenMin, double intenMax, String sn, int peakNr, double mArea, String tableName, int minRKey, int maxRKey, int operationId, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String xmlThreshold = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><operation name=\"Filter\"><filtertype>Threshold</filtertype><databasetable>" + tableName + "</databasetable><massmin>" + mzMin + "</massmin><massmax>" + mzMax + "</massmax><rettimemin>" + retTimeMin + "</rettimemin><rettimemax>" + retTimeMax + "</rettimemax><intenmin>" + intenMin + "</intenmin><sn>" + sn + "</sn><marea>" + mArea + "</marea><peaknumber>" + peakNr + "</peaknumber><minrkey>" + minRKey + "</minrkey><maxrkey>" + maxRKey + "</maxrkey></operation>";

        String createQuery = "INSERT INTO S_Metabolite_Operations VALUES(Null, '" + xmlThreshold + "', '" + mainUsername + "', '" + dateFormat.format(date) + "' )";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(createQuery);
        } catch (SQLException ex) {
        }

        int nextOpId = 0;
        String selectQuery = "SELECT MAX(Operation_Key) FROM S_Metabolite_Operations";

        try {
            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = st.executeQuery(selectQuery);

            while (result.next()) {

                nextOpId = result.getInt("MAX(Operation_Key)");

            }

        } catch (SQLException ex) {
        }

        if (nextOpId == 0) {
            nextOpId = 1;
        }

        ThresholdFilter thresholdFilter = new ThresholdFilter();

        thresholdFilter.filter(mainUsername, mzMin, mzMax, retTimeMin, retTimeMax, intenMin, intenMax, sn, peakNr, mArea, tableName, minRKey, maxRKey, operationId, nextOpId, connection);

    }

    /**
     *
     * @param mzMin
     * @param mzMax
     * @param retTimeMin
     * @param retTimeMax
     * @param intenMin
     * @param peakArea
     * @param peakNr
     * @param tableName
     * @param option
     * @param massRange
     * @param retTimeRange
     * @param firstTable
     * @param secondTable
     * @param minRKey
     * @param maxRKey
     * @param operationId
     * @param connection
     */
    public void metabolomeFiltering(Double mzMin, Double mzMax, Double retTimeMin, Double retTimeMax, Double intenMin, Double peakArea, Integer peakNr, String tableName, String option, Double massRange, Double retTimeRange, String firstTable, String secondTable, int minRKey, int maxRKey, int operationId, Connection connection) {

        String xmlThreshold = "";
        if (option.equals("threshold")) {
            xmlThreshold = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><operation name=\"Filter\"><filtertype>" + option + "</filtertype><databasetable>" + tableName + "</databasetable><massmin>" + mzMin + "</massmin><massmax>" + mzMax + "</massmax><rettimemin>" + retTimeMin + "</rettimemin><rettimemax>" + retTimeMax + "</rettimemax><intenmin>" + intenMin + "</intenmin><peakarea>" + peakArea + "</peakarea><peaknumber>" + peakNr + "</peaknumber><minrkey>" + minRKey + "</minrkey><maxrkey>" + maxRKey + "</maxrkey></operation>";

        } else if (option.equals("group")) {
            xmlThreshold = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><operation name=\"Filter\"><filtertype>" + option + "</filtertype><massrange>" + massRange + "</massrange><rettimerange>" + retTimeRange + "</rettimerange><firsttable>" + firstTable + "</firsttable><secondtable>" + secondTable + "</secondtable><minrkey>" + minRKey + "</minrkey><maxrkey>" + maxRKey + "</maxrkey><operationid>" + operationId + "</operationid></operation>";
        }
        String createQuery = "INSERT INTO S_Metabolite_Operations VALUES(Null, '" + xmlThreshold + "' )";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(createQuery);
        } catch (SQLException ex) {
        }

        int opId = 0;
        //String querySelect = "SELECT MAX(Next_value) FROM Mxbase_Nextid where Next_id = 'Next_Filterkey'";
        String selectQuery = "SELECT MAX(Operation_Key) FROM S_Metabolite_Operations";

        try {
            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = st.executeQuery(selectQuery);

            while (result.next()) {

                opId = result.getInt("MAX(Operation_Key)");

            }

        } catch (SQLException ex) {
        }

        if (opId == 0) {
            opId = 1;
        }

        String query = "";
        if (option.equals("threshold")) {

            if (tableName.equals("S_Metabolite_mining")) {
                query = "SELECT m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID";

                if (!mzMin.equals(0.0) && !mzMax.equals(0.0)) {
                    query += " WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "'";
                }
                if (!retTimeMin.equals(0.0) && !retTimeMax.equals(0.0)) {
                    query += " AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";
                }
                if (!intenMin.equals(0.0)) {
                    query += " AND m.I1 = '" + intenMin + "'";
                }
                if (!peakArea.equals(0.0)) {
                    query += " AND m.m_area = '" + peakArea + "'";
                }
                if (!peakNr.equals(0)) {
                    query += " AND m.peaks = '" + peakNr + "'";
                }
                if ((minRKey != 0) && (maxRKey != 0)) {
                    query = " AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                }

            } else if (tableName.equals("S_Metabolites_Filtered")) {
                query = "SELECT f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID";

                if (!mzMin.equals(0.0) && !mzMax.equals(0.0)) {
                    query += " WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "'";
                }
                if (!retTimeMin.equals(0.0) && !retTimeMax.equals(0.0)) {
                    query += " AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";
                }
                if (!intenMin.equals(0.0)) {
                    query += " AND f.I1 = '" + intenMin + "'";
                }
                if (!peakArea.equals(0.0)) {
                    query += " AND f.m_area = '" + peakArea + "'";
                }
                if (!peakNr.equals(0)) {
                    query += " AND f.peaks = '" + peakNr + "'";
                }
                if (operationId != 0) {
                    query += " AND f.Operation_Id = '" + operationId + "'";
                }
            } else {
                query = "SELECT META_Id, META_key, FeatureNr, Retentiontime, Charge, M1, M2, M3, M4, M5, mzlist, I1, I2, I3, I4, I5, Intensitylist, Rel_Intensitylist, Arealist, SNlist, Reslist, Deconvolutedlist, peaks, m_area, Analyticalsystem FROM  " + tableName + " ";

                if (!mzMin.equals(0.0) && !mzMax.equals(0.0)) {
                    query += " WHERE M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "'";
                }
                if (!retTimeMin.equals(0.0) && !retTimeMax.equals(0.0)) {
                    query += " AND Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";
                }
                if (!intenMin.equals(0.0)) {
                    query += " AND I1 = '" + intenMin + "'";
                }
                if (!peakArea.equals(0.0)) {
                    query += " AND m_area = '" + peakArea + "'";
                }
                if (!peakNr.equals(0)) {
                    query += " AND peaks = '" + peakNr + "'";
                }
            }

        } else if (option.equals(
                "group")) {

            MatrixFilter group = new MatrixFilter();
        }

        ThresholdFilter filtered = new ThresholdFilter();

        try {

            Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rslt = stmt.executeQuery(query);
            /*
             while (rslt.next()) {

             filtered.metabolomeFilteredSet(rslt.getString("META_Id"), rslt.getInt("META_key"), rslt.getInt("FeatureNr"), rslt.getDouble("Retentiontime"), rslt.getInt("Charge"),
             rslt.getDouble("M1"), rslt.getDouble("M2"), rslt.getDouble("M3"), rslt.getDouble("M4"), rslt.getDouble("M5"), rslt.getString("mzlist"), rslt.getDouble("I1"),
             rslt.getDouble("I2"), rslt.getDouble("I3"), rslt.getDouble("I4"), rslt.getDouble("I5"), rslt.getString("Intensitylist"), rslt.getString("Rel_Intensitylist"), rslt.getString("Arealist"), rslt.getString("SNlist"),
             rslt.getString("Reslist"), rslt.getString("Deconvolutedlist"), rslt.getInt("peaks"), rslt.getDouble("m_area"), rslt.getInt("Analyticalsystem"), rslt.getString("s_Comment"), opId, connection);
             }
             */
        } catch (SQLException ex) {

        }

    }

    /**
     *
     * @param mainUsername
     * @param minRKey
     * @param maxRKey
     * @param massRange
     * @param retTimeRange
     * @param tableName
     * @param operationId
     * @param connection
     */
    public void metabolomeCombinerQuery(String mainUsername, int minRKey, int maxRKey, Double massRange, Double retTimeRange, String tableName, int operationId, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String xmlThreshold = "<?xml version=\"1.0\" encoding=\"UTF-8\"?><operation name=\"Bucket\"><databasetable>" + tableName + "</databasetable><filteredoperationid>" + operationId + "</filteredoperationid><minrkey>" + minRKey + "</minrkey><maxrkey>" + maxRKey + "</maxrkey><massrange>" + massRange + "</massrange><rettimerange>" + retTimeRange + "</rettimerange></operation>";

        String createQuery = "INSERT INTO S_Metabolite_Operations VALUES(Null, '" + xmlThreshold + "', '" + mainUsername + "', '" + dateFormat.format(date) + "' )";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(createQuery);
        } catch (SQLException ex) {
        }

        int nextOpId = 0;
        //String querySelect = "SELECT MAX(Next_value) FROM Mxbase_Nextid where Next_id = 'Next_Filterkey'";
        String selectID = "SELECT MAX(Operation_Key) FROM S_Metabolite_Operations";

        try {
            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = st.executeQuery(selectID);

            while (result.next()) {

                nextOpId = result.getInt("MAX(Operation_Key)");

            }

        } catch (SQLException ex) {
        }
        if (nextOpId == 0) {
            nextOpId = 1;
        }
        try {
            Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);

            String selectQuery = "";

            if (tableName.equals("S_Metabolite_mining")) {
                selectQuery += "select R_Key, META_Id, META_key, Retentiontime, M1, m_area from S_Metabolite_mining";
                if ((minRKey != 0) && (maxRKey != 0)) {
                    selectQuery += " WHERE R_Key between '" + minRKey + "' and '" + maxRKey + "' ";
                }
            } else if (tableName.equals("S_Metabolites_Filtered")) {
                selectQuery += "select R_Key, META_Id, META_key, Retentiontime, M1, m_area from S_Metabolites_Filtered";
                if (operationId != 0) {
                    selectQuery += " WHERE Operation_Id = '" + operationId + "'";
                }
            } else {

                selectQuery += "select R_Key, META_Id, META_key, Retentiontime, M1, m_area from " + tableName + " ";
            }

            MetabolomeCombiner combiner = new MetabolomeCombiner();

            
            ResultSet rslt = stmt.executeQuery(selectQuery);
            List<Integer> rKeyList = new ArrayList<Integer>();
            List<String> metaIdList = new ArrayList<String>();
            List<Integer> mKeyList = new ArrayList<Integer>();
            List<Double> retTimeList = new ArrayList<Double>();
            List<Double> M1List = new ArrayList<Double>();
            List<Double> mAreaList = new ArrayList<Double>();
            while (rslt.next()) {
                int rKey = rslt.getInt("R_Key");
                rKeyList.add(rKey);
                String metaId = rslt.getString("META_Id");
                metaIdList.add(metaId);
                int mKey = rslt.getInt("META_key");
                mKeyList.add(mKey);
                double retTime = rslt.getDouble("Retentiontime");
                retTimeList.add(retTime);
                double M1 = rslt.getDouble("M1");
                M1List.add(M1);
                double mArea = rslt.getDouble("m_area");
                mAreaList.add(mArea);
            }
            combiner.metabolomeCombineMETA(mainUsername, rKeyList, metaIdList, mKeyList, retTimeList, M1List, mAreaList, minRKey, maxRKey, operationId, tableName, nextOpId, massRange, retTimeRange, connection);

        } catch (SQLException ex) {

        }
    }

    /**
     *
     * @param operationId
     * @param cmbBoxDistributionPhylLevel
     * @param connection
     * @return
     */
    public List<List<String>> generateCSV(int operationId, int cmbBoxDistributionPhylLevel, Connection connection) {

        List<List<String>> distributionMatrix = new ArrayList<List<String>>();

        List<String> title = new ArrayList<String>();
        List<String> matrix = new ArrayList<String>();
        List<String> details = new ArrayList<String>();

        List<Integer> bucketKeyList = new ArrayList<Integer>();
        List<Integer> metaRKeyList = new ArrayList<Integer>();
        List<Integer> metaKeyList = new ArrayList<Integer>();
        List<String> metaIdList = new ArrayList<String>();
        List<Integer> bucketNoList = new ArrayList<Integer>();
        List<Double> bucketAverageRetTime = new ArrayList<Double>();
        List<Double> bucketAverageMass = new ArrayList<Double>();

        String selectQuery = "";

        selectQuery = "SELECT * FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' ";

        try {
            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet result = st.executeQuery(selectQuery);

            while (result.next()) {

                int bucketKey = result.getInt("Bucket_Key");
                bucketKeyList.add(bucketKey);

                int metaRKey = result.getInt("R_Key");
                metaRKeyList.add(metaRKey);

                int metaKey = result.getInt("META_key");
                if (!metaKeyList.contains(metaKey)) {
                    metaKeyList.add(metaKey);
                }

                String metaId = result.getString("META_Id");
                if (!metaIdList.contains(metaId)) {
                    metaIdList.add(metaId);
                }

                int bucketNo = result.getInt("BucketNo");
                if (!bucketNoList.contains(bucketNo)) {
                    bucketNoList.add(bucketNo);
                }

            }

        } catch (SQLException ex) {
        }

        String averageQuery = "";

        for (int i = 0; i < bucketNoList.size(); i++) {
            averageQuery = "SELECT AverageRetTime, AverageMass FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' and BucketNo = '" + bucketNoList.get(i) + "' LIMIT 1 ";

            try {
                Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet result = st.executeQuery(averageQuery);

                while (result.next()) {

                    double bucketAverageR = result.getDouble("AverageRetTime");
                    bucketAverageRetTime.add(bucketAverageR);

                    double bucketAverageM = result.getDouble("AverageMass");
                    bucketAverageMass.add(bucketAverageM);


                }

            } catch (SQLException ex) {
            }
        }
        List<Bucket> buckets = new ArrayList<Bucket>();

        for (int i = 0; i < bucketNoList.size(); i++) {

            Bucket bucket = new Bucket();

            bucket.setBucketNo(bucketNoList.get(i));

            String bucketSelect = "";

            List<Integer> metaKeySelect = new ArrayList<Integer>();
            List<Double> mAreaSelect = new ArrayList<Double>();
            List<Integer> mAreaCount = new ArrayList<Integer>();

            for (int k = 0; k < metaKeyList.size(); k++) {

                String areaCount = "";

                areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaKeyList.get(k) + "' ";


                try {
                    Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                    ResultSet result = st.executeQuery(areaCount);

                    while (result.next()) {

                        int count = result.getInt("COUNT(Bucket_Key)");
                        mAreaCount.add(count);

                        int metaKey = result.getInt("META_key");
                        metaKeySelect.add(metaKey);

                        double mArea = result.getDouble("m_area");
                        mAreaSelect.add(mArea);
                    }

                } catch (SQLException ex) {
                }
            }

            bucket.setMetaKeyCollection(metaKeySelect);
            bucket.setmAreaCollection(mAreaSelect);
            bucket.setmAreaCount(mAreaCount);

            ////
            buckets.add(bucket);
        }

        ////
        //Matrix
        List<DistributionMatrixItem> matrixItemList = new ArrayList<DistributionMatrixItem>();

        List<Map<String, List<Integer>>> speciesAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> genusAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> familyAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> suborderAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> orderAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> classAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> phylumAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> kingdomAll = new ArrayList<Map<String, List<Integer>>>();
        List<Map<String, List<Integer>>> domainAll = new ArrayList<Map<String, List<Integer>>>();
        //List<List<String>> genusAllAll = new ArrayList<List<String>>();

        Map<String, List<Integer>> speciesList = new HashMap<String, List<Integer>>();
        List<String> species = new ArrayList<String>();

        Map<String, List<Integer>> genusList = new HashMap<String, List<Integer>>();
        List<String> genus = new ArrayList<String>();

        Map<String, List<Integer>> familyList = new HashMap<String, List<Integer>>();
        List<String> family = new ArrayList<String>();

        Map<String, List<Integer>> suborderList = new HashMap<String, List<Integer>>();
        List<String> suborder = new ArrayList<String>();

        Map<String, List<Integer>> orderList = new HashMap<String, List<Integer>>();
        List<String> order = new ArrayList<String>();

        Map<String, List<Integer>> classList = new HashMap<String, List<Integer>>();
        List<String> classs = new ArrayList<String>();

        Map<String, List<Integer>> phylumList = new HashMap<String, List<Integer>>();
        List<String> phylum = new ArrayList<String>();

        Map<String, List<Integer>> kingdomList = new HashMap<String, List<Integer>>();
        List<String> kingdom = new ArrayList<String>();

        Map<String, List<Integer>> domainList = new HashMap<String, List<Integer>>();
        List<String> domain = new ArrayList<String>();

        List<Integer> speciesKeyAll = new ArrayList<Integer>();
        List<Integer> genusKeyAll = new ArrayList<Integer>();
        List<Integer> familyKeyAll = new ArrayList<Integer>();
        List<Integer> suborderKeyAll = new ArrayList<Integer>();
        List<Integer> orderKeyAll = new ArrayList<Integer>();
        List<Integer> classKeyAll = new ArrayList<Integer>();
        List<Integer> phylumKeyAll = new ArrayList<Integer>();
        List<Integer> kingdomKeyAll = new ArrayList<Integer>();
        List<Integer> domainKeyAll = new ArrayList<Integer>();

        for (int k = 0; k < metaKeyList.size(); k++) {

            DistributionMatrixItem matrixItem = new DistributionMatrixItem();

            String matrixQuery = "select t.s_Strain, t.s_MxID, S_Domain.s_Domain, S_Kingdom.s_Kingdom, S_Phylum.s_Phylum, S_Class.s_Class, S_Order.s_Order1, S_Suborder.s_Suborder, S_Family.s_Family, S_Genus.s_Genus, S_Species.s_Species, S_Subsp.s_Subsp, t.s_Strain from S_StrainMasterTable t, S_Metabolome_data d, S_Metabolite_Buckets b, S_Domain, S_Kingdom, S_Phylum, S_Class, S_Order, S_Suborder, S_Family, S_Genus, S_Species, S_Subsp where t.s_Domain = S_Domain.s_DomNr and t.s_Kingdom = S_Kingdom.s_KingdomNr and t.s_Phylum = S_Phylum.s_PhylumNr and t.s_Class = S_Class.s_ClassNr and t.s_Order = S_Order.s_Order and t.s_Suborder = S_Suborder.s_SuborderNr and t.s_Family = S_Family.s_FamilyNr and t.s_Genus = S_Genus.s_GenusNr and t.s_species = S_Species.s_SpeciesNr and t.s_Subspecies = S_Subsp.s_SubspNr and d.META_key = '" + metaKeyList.get(k) + "' and d.META_key = b.META_key and b.Operation_Id = '" + operationId + "' and d.s_MXID = t.s_MxID LIMIT 1;";

            List<Integer> speciesKey = new ArrayList<Integer>();
            List<Integer> genusKey = new ArrayList<Integer>();
            List<Integer> familyKey = new ArrayList<Integer>();
            List<Integer> suborderKey = new ArrayList<Integer>();
            List<Integer> orderKey = new ArrayList<Integer>();
            List<Integer> classKey = new ArrayList<Integer>();
            List<Integer> phylumKey = new ArrayList<Integer>();
            List<Integer> kingdomKey = new ArrayList<Integer>();
            List<Integer> domainKey = new ArrayList<Integer>();

            try {
                Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet result = st.executeQuery(matrixQuery);

                while (result.next()) {

                    String s_Domain = result.getString("s_Domain");
                    String s_Kingdom = result.getString("s_Kingdom");
                    String s_Phylum = result.getString("s_Phylum");
                    String s_Class = result.getString("s_Class");
                    String s_Order1 = result.getString("s_Order1");
                    String s_Suborder = result.getString("s_Suborder");
                    String s_Family = result.getString("s_Family");
                    String s_Genus = result.getString("s_Genus");
                    String s_Species = result.getString("s_Species");

                    if (cmbBoxDistributionPhylLevel == 8) {

                        domainKeyAll.add(metaKeyList.get(k));
                        domainList.put(s_Domain, domainKeyAll);
                        kingdomKeyAll.add(metaKeyList.get(k));
                        kingdomList.put(s_Kingdom, kingdomKeyAll);
                        phylumKeyAll.add(metaKeyList.get(k));
                        phylumList.put(s_Phylum, phylumKeyAll);
                        classKeyAll.add(metaKeyList.get(k));
                        classList.put(s_Class, classKeyAll);
                        orderKeyAll.add(metaKeyList.get(k));
                        orderList.put(s_Order1, orderKeyAll);
                        suborderKeyAll.add(metaKeyList.get(k));
                        suborderList.put(s_Suborder, suborderKeyAll);
                        familyKeyAll.add(metaKeyList.get(k));
                        familyList.put(s_Family, familyKeyAll);
                        genusKeyAll.add(metaKeyList.get(k));
                        genusList.put(s_Genus, genusKeyAll);

                        if (!speciesList.containsKey(s_Species)) {
                            speciesKey.add(metaKeyList.get(k));
                            speciesList.put(s_Species, speciesKey);

                          
                        } else if (speciesList.containsKey(s_Species)) {
                            List<Integer> existingSpecies = speciesList.get(s_Species);
                            existingSpecies.add(metaKeyList.get(k));
                            speciesList.put(s_Species, existingSpecies);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 7) {

                        domainKeyAll.add(metaKeyList.get(k));
                        domainList.put(s_Domain, domainKeyAll);
                        kingdomKeyAll.add(metaKeyList.get(k));
                        kingdomList.put(s_Kingdom, kingdomKeyAll);
                        phylumKeyAll.add(metaKeyList.get(k));
                        phylumList.put(s_Phylum, phylumKeyAll);
                        classKeyAll.add(metaKeyList.get(k));
                        classList.put(s_Class, classKeyAll);
                        orderKeyAll.add(metaKeyList.get(k));
                        orderList.put(s_Order1, orderKeyAll);
                        suborderKeyAll.add(metaKeyList.get(k));
                        suborderList.put(s_Suborder, suborderKeyAll);
                        familyKeyAll.add(metaKeyList.get(k));
                        familyList.put(s_Family, familyKeyAll);

                        if (!genusList.containsKey(s_Genus)) {
                            genusKey.add(metaKeyList.get(k));
                            genusList.put(s_Genus, genusKey);

                        } else if (genusList.containsKey(s_Genus)) {
                            List<Integer> existingGenus = genusList.get(s_Genus);
                            existingGenus.add(metaKeyList.get(k));
                            genusList.put(s_Genus, existingGenus);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 6) {

                        domainKeyAll.add(metaKeyList.get(k));
                        domainList.put(s_Domain, domainKeyAll);
                        kingdomKeyAll.add(metaKeyList.get(k));
                        kingdomList.put(s_Kingdom, kingdomKeyAll);
                        phylumKeyAll.add(metaKeyList.get(k));
                        phylumList.put(s_Phylum, phylumKeyAll);
                        classKeyAll.add(metaKeyList.get(k));
                        classList.put(s_Class, classKeyAll);
                        orderKeyAll.add(metaKeyList.get(k));
                        orderList.put(s_Order1, orderKeyAll);
                        suborderKeyAll.add(metaKeyList.get(k));
                        suborderList.put(s_Suborder, suborderKeyAll);

                        if (!familyList.containsKey(s_Family)) {
                            familyKey.add(metaKeyList.get(k));
                            familyList.put(s_Family, familyKey);

                        } else if (familyList.containsKey(s_Family)) {
                            List<Integer> existingFamily = familyList.get(s_Family);
                            existingFamily.add(metaKeyList.get(k));
                            familyList.put(s_Family, existingFamily);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 5) {

                        domainKeyAll.add(metaKeyList.get(k));
                        domainList.put(s_Domain, domainKeyAll);
                        kingdomKeyAll.add(metaKeyList.get(k));
                        kingdomList.put(s_Kingdom, kingdomKeyAll);
                        phylumKeyAll.add(metaKeyList.get(k));
                        phylumList.put(s_Phylum, phylumKeyAll);
                        classKeyAll.add(metaKeyList.get(k));
                        classList.put(s_Class, classKeyAll);
                        orderKeyAll.add(metaKeyList.get(k));
                        orderList.put(s_Order1, orderKeyAll);

                        if (!suborderList.containsKey(s_Suborder)) {
                            suborderKey.add(metaKeyList.get(k));
                            suborderList.put(s_Suborder, suborderKey);

                        } else if (suborderList.containsKey(s_Suborder)) {
                            List<Integer> existingSuborder = suborderList.get(s_Suborder);
                            existingSuborder.add(metaKeyList.get(k));
                            suborderList.put(s_Suborder, existingSuborder);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 4) {

                        if (!orderList.containsKey(s_Order1)) {
                            orderKey.add(metaKeyList.get(k));
                            orderList.put(s_Order1, orderKey);

                            domainKeyAll.add(metaKeyList.get(k));
                            domainList.put(s_Domain, domainKeyAll);
                            kingdomKeyAll.add(metaKeyList.get(k));
                            kingdomList.put(s_Kingdom, kingdomKeyAll);
                            phylumKeyAll.add(metaKeyList.get(k));
                            phylumList.put(s_Phylum, phylumKeyAll);
                            classKeyAll.add(metaKeyList.get(k));
                            classList.put(s_Class, classKeyAll);

                        } else if (orderList.containsKey(s_Order1)) {
                            List<Integer> existingOrder = orderList.get(s_Order1);
                            existingOrder.add(metaKeyList.get(k));
                            orderList.put(s_Order1, existingOrder);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 3) {

                        if (!classList.containsKey(s_Class)) {
                            classKey.add(metaKeyList.get(k));
                            classList.put(s_Class, classKey);

                            domainKeyAll.add(metaKeyList.get(k));
                            domainList.put(s_Domain, domainKeyAll);
                            kingdomKeyAll.add(metaKeyList.get(k));
                            kingdomList.put(s_Kingdom, kingdomKeyAll);
                            phylumKeyAll.add(metaKeyList.get(k));
                            phylumList.put(s_Phylum, phylumKeyAll);

                        } else if (classList.containsKey(s_Class)) {
                            List<Integer> existingClass = classList.get(s_Class);
                            existingClass.add(metaKeyList.get(k));
                            classList.put(s_Class, existingClass);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 2) {

                        if (!phylumList.containsKey(s_Phylum)) {
                            phylumKey.add(metaKeyList.get(k));
                            phylumList.put(s_Phylum, phylumKey);

                            domainKeyAll.add(metaKeyList.get(k));
                            domainList.put(s_Domain, domainKeyAll);
                            kingdomKeyAll.add(metaKeyList.get(k));
                            kingdomList.put(s_Kingdom, kingdomKeyAll);

                        } else if (phylumList.containsKey(s_Phylum)) {
                            List<Integer> existingPhylum = phylumList.get(s_Phylum);
                            existingPhylum.add(metaKeyList.get(k));
                            phylumList.put(s_Phylum, existingPhylum);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 1) {

                        if (!kingdomList.containsKey(s_Kingdom)) {
                            kingdomKey.add(metaKeyList.get(k));
                            kingdomList.put(s_Kingdom, kingdomKey);

                            domainKeyAll.add(metaKeyList.get(k));
                            domainList.put(s_Domain, domainKeyAll);

                        } else if (kingdomList.containsKey(s_Kingdom)) {
                            List<Integer> existingKingdom = kingdomList.get(s_Kingdom);
                            existingKingdom.add(metaKeyList.get(k));
                            kingdomList.put(s_Kingdom, existingKingdom);
                        }

                    } else if (cmbBoxDistributionPhylLevel == 0) {

                        if (!domainList.containsKey(s_Domain)) {
                            domainKey.add(metaKeyList.get(k));
                            domainList.put(s_Domain, domainKey);
                        } else if (domainList.containsKey(s_Domain)) {
                            List<Integer> existingDomain = domainList.get(s_Domain);
                            existingDomain.add(metaKeyList.get(k));
                            domainList.put(s_Domain, existingDomain);
                        }

                    }
                }

            } catch (SQLException ex) {
            }

        }

        speciesAll.add(speciesList);
        genusAll.add(genusList);
        familyAll.add(familyList);
        suborderAll.add(suborderList);
        orderAll.add(orderList);
        classAll.add(classList);
        phylumAll.add(phylumList);
        kingdomAll.add(kingdomList);
        domainAll.add(domainList);

        String detailsKnime = "";
        
       

        int fix = 0;
        for (int i = 0; i < bucketNoList.size(); i++) {

            List<List<String>> metaTitles = new ArrayList<List<String>>();
            String areaCount = "";

            List<String> countAll = new ArrayList<String>();

            List<String> metaTitle = new ArrayList<String>();

            if (cmbBoxDistributionPhylLevel == 8) {

                for (List<Integer> meta : speciesList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
            

                }

                details.add(detailsKnime);


                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
             
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 7) {

                for (List<Integer> meta : genusList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";

                }

                details.add(detailsKnime);

                for (String meta : metaTitle) {

                }

                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 6) {

                for (List<Integer> meta : familyList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
                    //bucketNo.add(value);

                }

                details.add(detailsKnime);

                for (String meta : metaTitle) {

                }

                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 5) {

                for (List<Integer> meta : suborderList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
                    //bucketNo.add(value);

                }

                details.add(detailsKnime);

                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 4) {

                for (List<Integer> meta : orderList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
                    //bucketNo.add(value);

                }

                details.add(detailsKnime);


                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 3) {

                for (List<Integer> meta : classList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");
                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
                    //bucketNo.add(value);

                }

                details.add(detailsKnime);

                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 2) {

                for (List<Integer> meta : phylumList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";
                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
                    //bucketNo.add(value);

                }

                details.add(detailsKnime);

                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 1) {

                for (List<Integer> meta : kingdomList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
                    //bucketNo.add(value);

                }

                details.add(detailsKnime);


                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            } else if (cmbBoxDistributionPhylLevel == 0) {

                for (List<Integer> meta : domainList.values()) {
                    String total = "";
                    int count = 0;
                    String metas = "";
                    for (int metaEach : meta) {
                        metas += "META" + metaEach + "/";
                        areaCount = "SELECT COUNT(Bucket_Key), META_key, m_area FROM S_Metabolite_Buckets WHERE Operation_Id = '" + operationId + "' AND BucketNo = '" + bucketNoList.get(i) + "' and META_key = '" + metaEach + "' ";

                        try {
                            Statement st = (Statement) connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                            ResultSet result = st.executeQuery(areaCount);

                            while (result.next()) {

                                count = result.getInt("COUNT(Bucket_Key)");

                                total += count + ",";
                            }
                        } catch (Exception ex) {

                        }
                    }
                    metaTitle.add(metas);
                    countAll.add(total);

                }

                String valueCount = "";

                detailsKnime = "";

                detailsKnime += bucketNoList.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageMass.get(i);

                detailsKnime += "#";

                detailsKnime += bucketAverageRetTime.get(i);

                detailsKnime += "#";

                for (String count : countAll) {

                    String[] countArray = count.split(",");
                    int value = 0;
                    for (String array : countArray) {
                        value += Integer.parseInt(array);
                    }

                    detailsKnime += value;
                    detailsKnime += "%";
                    //bucketNo.add(value);

                }

                details.add(detailsKnime);

                if (fix == 0) {

                    String titleMeta = "";
                    for (int m = 0; m < metaTitle.size(); m++) {
                        fix++;
                        titleMeta += metaTitle.get(m);

                        titleMeta += "#";
                        //String[] titleMet = metaTitle.get(m).split(",");

                        //for (String tit : titleMet) {
                        //  titleMeta += tit;
                        //titleMeta += "#";
                        //}
                    }
                    title.add(titleMeta);

                }

                //}
            }
        }

        String[] titSize;
        int len = 0;

        for (String tit : title) {
            titSize = tit.split("#");

            len = titSize.length;
        }

        String speciesString = " ";
        String genusString = " ";
        String familyString = " ";
        String suborderString = " ";
        String orderString = " ";
        String classString = " ";
        String phylumString = " ";
        String kingdomString = " ";
        String domainString = " ";

        if (speciesList.size() != 0) {
            speciesString = "Species#";
            for (String spe : speciesList.keySet()) {
                if (speciesList.keySet().size() == 1) {
                    for (int i = 0; i < len; i++) {
                        speciesString += spe;
                        speciesString += "%";
                    }
                } else {
                    speciesString += spe;
                    speciesString += "%";
                }
            }
        }

        if (genusList.size() != 0) {
            genusString = "Genus#";
            for (String tit : title) {
                for (String gen : genusList.keySet()) {

                    if (genusList.keySet().size() == 1) {
                        for (int i = 0; i < len; i++) {
                            genusString += gen;
                            genusString += "%";
                        }
                    } else {
                        genusString += gen;
                        genusString += "%";
                    }
                }
            }
        }

        if (familyList.size() != 0) {
            familyString = "Family#";
            for (String tit : title) {
                for (String fam : familyList.keySet()) {

                    if (familyList.keySet().size() == 1) {
                        for (int i = 0; i < len; i++) {
                            familyString += fam;
                            familyString += "%";
                        }
                    } else {
                        familyString += fam;
                        familyString += "%";
                    }
                }
            }
        }

        if (suborderList.size() != 0) {
            suborderString = "Suborder#";
            for (String tit : title) {
                for (String sub : suborderList.keySet()) {

                    if (suborderList.keySet().size() == 1) {
                        for (int i = 0; i < len; i++) {
                            suborderString += sub;
                            suborderString += "%";
                        }
                    } else {
                        suborderString += sub;
                        suborderString += "%";
                    }
                }
            }
        }
        if (orderList.size() != 0) {
            orderString = "Order#";
            for (String tit : title) {
                for (String ord : orderList.keySet()) {

                    if (orderList.keySet().size() == 1) {
                        for (int i = 0; i < len; i++) {
                            orderString += ord;
                            orderString += "%";
                        }
                    } else {
                        orderString += ord;
                        orderString += "%";
                    }
                }
            }
        }
        if (classList.size() != 0) {
            classString = "Class#";
            for (String tit : title) {
                for (String cla : classList.keySet()) {

                    if (classList.keySet().size() == 1) {
                        for (int i = 0; i < len; i++) {
                            classString += cla;
                            classString += "%";
                        }
                    } else {
                        classString += cla;
                        classString += "%";
                    }
                }
            }
        }
        if (phylumList.size() != 0) {
            phylumString = "Phylum#";
            for (String tit : title) {
                for (String phy : phylumList.keySet()) {

                    if (phylumList.keySet().size() == 1) {
                        for (int i = 0; i < len; i++) {
                            phylumString += phy;
                            phylumString += "%";
                        }
                    } else {
                        phylumString += phy;
                        phylumString += "%";
                    }
                }
            }
        }
        if (kingdomList.size() != 0) {
            kingdomString = "Kingdom#";
            for (String tit : title) {
                for (String king : kingdomList.keySet()) {

                    if (kingdomList.keySet().size() == 1) {
                        for (int i = 0; i < len; i++) {
                            kingdomString += king;
                            kingdomString += "%";
                        }
                    } else {
                        kingdomString += king;
                        kingdomString += "%";
                    }
                }
            }
        }
        if (domainList.size() != 0) {
            domainString = "Domain#";
            for (String tit : title) {
                for (String dom : domainList.keySet()) {

                    if (domainList.keySet().size() == 1) {

                        for (int i = 0; i < len; i++) {
                            domainString += dom;
                            domainString += "%";
                        }
                    } else {
                        domainString += dom;
                        domainString += "%";
                    }
                }
            }
        }
        if (!domainString.equals(" ")) {
            matrix.add(domainString);
        }
        if (!kingdomString.equals(" ")) {
            matrix.add(kingdomString);
        }
        if (!phylumString.equals(" ")) {
            matrix.add(phylumString);
        }
        if (!classString.equals(" ")) {
            matrix.add(classString);
        }
        if (!orderString.equals(" ")) {
            matrix.add(orderString);
        }
        if (!suborderString.equals(" ")) {
            matrix.add(suborderString);
        }
        if (!familyString.equals(" ")) {
            matrix.add(familyString);
        }
        if (!genusString.equals(" ")) {
            matrix.add(genusString);
        }
        if (!speciesString.equals(" ")) {
            matrix.add(speciesString);
        }

        distributionMatrix.add(matrix);
        distributionMatrix.add(title);
        distributionMatrix.add(details);

        for (int i = 0; i < distributionMatrix.size(); i++) {

            for (int m = 0; m < distributionMatrix.get(i).size(); m++) {
            }
        }

        return distributionMatrix;
    }

}
