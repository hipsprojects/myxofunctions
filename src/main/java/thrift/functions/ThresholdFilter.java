/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nibo01
 */
public class ThresholdFilter {

    public void filter(String mainUsername, double mzMin, double mzMax, double retTimeMin, double retTimeMax, double intenMin, double intenMax, String sn, int peakNr, double mArea, String tableName, int minRKey, int maxRKey, int operationId, int nextOpId, Connection connection) {

        String query = "";

        if (tableName.equalsIgnoreCase("S_Metabolite_mining")) {

            if ((mzMin != 0.0) && (mzMax != 0.0)) {
                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "'";
                if ((retTimeMin != 0.0) && (retTimeMax != 0.0)) {
                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT  m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT  m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    }
                                }
                            } //peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    }
                                }
                            }
                        } // sn null 
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' ";
                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "'";
                                    }
                                }

                            }//peakNr null 
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "'";
                                    }
                                }
                            }
                        }
                    } //intenMin intenMax null 
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "'";

                                    }
                                }
                            }//peakNr null 
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";

                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "'";

                                    }
                                }
                            }
                        }//sn null
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' ";

                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";

                                    }
                                }
                            }
                        }
                    }
                } //retTimeMin retTimeMax null
                else {
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' ";

                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "'";
                                    } else {

                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "'";
                                    }
                                }
                            }
                        }//sn null
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";

                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";

                                    }
                                }
                            }
                        }
                    }//intenMin intenMax null
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "'";
                                    }
                                }
                            }//peakNr null 
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.SNlist = '" + sn + "'";

                                    }
                                }
                            }
                        }//sn null 
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.peaks = '" + peakNr + "' ";

                                    }
                                }
                            }
                        }
                    }
                }
            } //mzMin MzMax null
            else {
                if ((retTimeMin != 0.0) && (retTimeMax != 0.0)) {
                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";

                                    }
                                }
                            }//peakNr
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "'";

                                    }
                                }
                            }
                        }//sn null 
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "'";

                                    }
                                }
                            }
                        }
                    }//intenMin intenMax null 
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' ";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";

                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.SNlist = '" + sn + "' ";

                                    }
                                }
                            }
                        }//sn null
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "'";

                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' ";

                                    }
                                }
                            }
                        }
                    }
                }//retTimeMin retTimeMax null 
                else {
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    }
                                }
                            }
                        }
                    }//intenMin intenMax null
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' ";

                                    }
                                }//mArea null 
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";

                                    }
                                }
                            }//peakNr null 
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.m_area = '" + mArea + "' ";
                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        else {
                            if (peakNr != 0) {
                                query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.peaks = '" + peakNr + "' AND m.m_area = '" + mArea + "'";

                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.peaks = '" + peakNr + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.peaks = '" + peakNr + "'";

                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.m_area = '" + mArea + "'";
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.m_area = '" + mArea + "' AND m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.m_area = '" + mArea + "' ";
                                    }
                                }//mArea null
                                else {
                                    if ((minRKey != 0) && (maxRKey != 0)) {
                                        query = "SELECT m.R_Key, m.META_Id, m.META_key, m.FeatureNr, m.Retentiontime, m.Charge, m.M1, m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID WHERE m.R_Key BETWEEN '" + minRKey + "' AND '" + maxRKey + "'";
                                    } else {
                                        //Should not occur!!
                                    }
                                }
                            }
                        }
                    }
                }
            }

        } else if (tableName.equalsIgnoreCase("S_Metabolites_Filtered")) {

            if ((mzMin != 0.0) && (mzMax != 0.0)) {
                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "'";
                if ((retTimeMin != 0.0) && (retTimeMax != 0.0)) {
                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals("")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                                }
                            }
                        }
                    }//intenMin intenMax null
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' ";
                                }
                            }
                        }
                    }
                }//retTimeMin retTimeMax null sdsds
                else {
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals("")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "'  AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                                }
                            }
                        }
                    }//intenMin intenMax null
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.M1 BETWEEN '" + mzMin + "' AND '" + mzMax + "' ";
                                }
                            }
                        }
                    }
                }
            }//mzMin mzMax null
            else {
                //
                if ((retTimeMin != 0.0) && (retTimeMax != 0.0)) {
                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "'";
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals(" ")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE  f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                                }
                            }
                        }
                    }//intenMin intenMax null
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Retentiontime BETWEEN '" + retTimeMin + "' AND '" + retTimeMax + "' ";
                                }
                            }
                        }
                    }
                }//retTimeMin retTimeMax null sdsds
                else {
                    if ((intenMin != 0.0) && (intenMax != 0.0)) {
                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                        if (!sn.equals("")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.I1 BETWEEN '" + intenMin + "' AND '" + intenMax + "' ";
                                }
                            }
                        }
                    }//intenMin intenMax null
                    else {
                        if (!sn.equals("")) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "'";
                            if (peakNr != 0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE AND f.SNlist = '" + sn + "' AND m.peaks = '" + peakNr + "' ";
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE AND f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.peaks = '" + peakNr + "' ";
                                    }
                                }
                            }//peakNr null
                            else {
                                if (mArea != 0.0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.m_area = '" + mArea + "'";
                                    }
                                } //mArea null
                                else {
                                    if (operationId != 0) {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' AND f.Operation_Id = '" + operationId + "'";
                                    } //minRKey maxRKey null 
                                    else {
                                        query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.SNlist = '" + sn + "' ";
                                    }
                                }
                            }
                        }//sn null
                        if (peakNr != 0) {
                            query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE m.peaks = '" + peakNr + "' ";
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.peaks = '" + peakNr + "' AND f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.peaks = '" + peakNr + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.peaks = '" + peakNr + "' ";
                                }
                            }
                        }//peakNr null
                        else {
                            if (mArea != 0.0) {
                                query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.m_area = '" + mArea + "'";
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.m_area = '" + mArea + "' AND f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.m_area = '" + mArea + "'";
                                }
                            } //mArea null
                            else {
                                if (operationId != 0) {
                                    query = "SELECT f.R_Key, f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Operation_Id = '" + operationId + "'";
                                } //minRKey maxRKey null 
                                else {
                                    //Should not occur!!
                                    //query = "SELECT f.META_Id, f.META_key, f.FeatureNr, f.Retentiontime, f.Charge, f.M1, f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID";
                                }
                            }
                        }
                    }
                }
            }

        } 
   

        try {

            boolean check = false;
            Statement stmt = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rslt = stmt.executeQuery(query);

            while (rslt.next()) {
                check = true;

                metabolomeFilteredSet(mainUsername, rslt.getString("R_Key"), rslt.getString("META_Id"), rslt.getInt("META_key"), rslt.getInt("FeatureNr"), rslt.getDouble("Retentiontime"), rslt.getInt("Charge"),
                        rslt.getDouble("M1"), rslt.getDouble("M2"), rslt.getDouble("M3"), rslt.getDouble("M4"), rslt.getDouble("M5"), rslt.getString("mzlist"), rslt.getDouble("I1"),
                        rslt.getDouble("I2"), rslt.getDouble("I3"), rslt.getDouble("I4"), rslt.getDouble("I5"), rslt.getString("Intensitylist"), rslt.getString("Rel_Intensitylist"), rslt.getString("Arealist"), rslt.getString("SNlist"),
                        rslt.getString("Reslist"), rslt.getString("Deconvolutedlist"), rslt.getInt("peaks"), rslt.getDouble("m_area"), rslt.getInt("Analyticalsystem"), rslt.getString("s_Comment"), nextOpId, connection);

            }

            /*
            if (check) {
                insertIntoMetabolomeResult(mainUsername, rslt.getString("META_Id"), "It returns results!", nextOpId, connection);
            } else if (check == false) {
                insertIntoMetabolomeResult(mainUsername, "Null", "It does not return any results!", nextOpId, connection);
            }
            */
        } catch (SQLException ex) {

        }

    }

    public void metabolomeFilteredSet(String mainUsername, String R_Key, String META_Id, int META_key, int FeatureNr, double RetentionTime, int Charge, double M1, double M2, double M3, double M4, double M5, String mzlist, double I1, double I2, double I3, double I4, double I5, String Intensitylist, String Rel_Intensitylist, String Arealist, String SNlist, String Reslist, String Deconvolutedlist, int peaks, double m_area, int Analyticalsystem, String s_Comment, int operationID, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String insertQuery = "";
        insertQuery = "INSERT INTO S_Metabolites_Filtered VALUES(Null, '" + R_Key + "','" + META_Id + "','" + META_key + "','" + FeatureNr + "','" + RetentionTime + "','" + Charge + "','" + M1 + "','" + M2 + "','" + M3 + "','" + M4 + "','" + M5 + "','" + mzlist + "','" + I1 + "','" + I2 + "','" + I3 + "','" + I4 + "','" + I5 + "','" + Intensitylist + "','" + Rel_Intensitylist + "','" + Arealist + "','" + SNlist + "','" + Reslist + "','" + Deconvolutedlist + "','" + peaks + "','" + m_area + "','" + Analyticalsystem + "','" + s_Comment + "','" + operationID + "','" + mainUsername + "','" + dateFormat.format(date) + "')";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(insertQuery);
        } catch (SQLException ex) {
        }

    }

    public void insertIntoMetabolomeResult(String mainUsername, String META_Id, String Details, int opId, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String insertQuery = "";

        insertQuery = "INSERT INTO S_Metabolite_Results VALUES(Null, 'Threshold Filtering','" + META_Id + "','" + opId + "','" + Details + "','" + mainUsername + "', '" + dateFormat.format(date) + "' )";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(insertQuery);
        } catch (SQLException ex) {
        }
    }

}
