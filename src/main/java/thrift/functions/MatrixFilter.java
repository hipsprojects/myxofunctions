/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author nibo01
 */
public class MatrixFilter {

    public void filter(String mainUsername, Double massRange, Double retTimeRange, int nextOpId, String mainTable, String matrixTable, int operationId, String metaKeyList, Connection connection) {

        boolean check = false;

        String queryMainTable = "";
        String querySubTable = "";

        if (mainTable.equalsIgnoreCase("S_Metabolites_Filtered")) {

            if (operationId != 0) {
                queryMainTable = "SELECT f.R_Key as 'R_Key', f.META_Id as 'META_Id', f.META_key as 'META_key', f.FeatureNr as 'FeatureNr', f.Retentiontime as 'Retentiontime', f.Charge as 'Charge', f.M1 as 'M1', f.M2 as 'M2', f.M3 as 'M3', f.M4 as 'M4', f.M5 as 'M5', f.mzlist as 'mzlist', f.I1 as 'I1', f.I2 as 'I2', f.I3 as 'I3', f.I4 as 'I4', f.I5 as 'I5', f.Intensitylist as 'Intensitylist', f.Rel_Intensitylist as 'Rel_Intensitylist', f.Arealist as 'Arealist', f.SNlist as 'SNlist', f.Reslist as 'Reslist', f.Deconvolutedlist as 'Deconvolutedlist', f.peaks as 'peaks', f.m_area as 'm_area', f.Analyticalsystem as 'Analyticalsystem', d.s_Comment as 's_Comment' FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Operation_Id = '" + operationId + "'";
            } else {
            }

        } else if (mainTable.equalsIgnoreCase("S_Metabolite_mining")) {
            queryMainTable = "SELECT m.R_Key as 'R_Key', m.META_Id as 'META_Id', m.META_key as 'META_key', m.FeatureNr as 'FeatureNr', m.Retentiontime as 'Retentiontime', m.Charge as 'Charge', m.M1 as 'M1', m.M2 as 'M2', m.M3 as 'M3', m.M4 as 'M4', m.M5 as 'M5', m.mzlist as 'mzlist', m.I1 as 'I1', m.I2 as 'I2', m.I3 as 'I3', m.I4 as 'I4', m.I5 as 'I5', m.Intensitylist as 'Intensitylist', m.Rel_Intensitylist as 'Rel_Intensitylist', m.Arealist as 'Arealist', m.SNlist as 'SNlist', m.Reslist as 'Reslist', m.Deconvolutedlist as 'Deconvolutedlist', m.peaks as 'peaks', m.m_area as 'm_area', m.Analyticalsystem as 'Analyticalsystem', d.s_Comment as 's_Comment' FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID ";
        } else {
            queryMainTable = "SELECT R_Key as 'R_Key', META_Id as 'META_Id', META_key as 'META_key', FeatureNr as 'FeatureNr', Retentiontime as 'Retentiontime', Charge as 'Charge', M1 as 'M1', M2 as 'M2', M3 as 'M3', M4 as 'M4', M5 as 'M5', mzlist as 'mzlist', I1 as 'I1', I2 as 'I2', I3 as 'I3', I4 as 'I4', I5 as 'I5', Intensitylist as 'Intensitylist', Rel_Intensitylist as 'Rel_Intensitylist', Arealist as 'Arealist', SNlist as 'SNlist', Reslist as 'Reslist', Deconvolutedlist as 'Deconvolutedlist', peaks as 'peaks', m_area as 'm_area', Analyticalsystem as 'Analyticalsystem', Media_type as 's_Comment' FROM " + mainTable + " ";
        }

        if (matrixTable.equalsIgnoreCase("S_Metabolites_Filtered")) {

            if (operationId != 0) {
                querySubTable = "SELECT f.R_Key as 'R_Key', f.META_Id as 'META_Id', f.META_key as 'META_key', f.FeatureNr as 'FeatureNr', f.Retentiontime as 'Retentiontime', f.Charge as 'Charge', f.M1 as 'M1', f.M2, f.M3, f.M4, f.M5, f.mzlist, f.I1, f.I2, f.I3, f.I4, f.I5, f.Intensitylist, f.Rel_Intensitylist, f.Arealist, f.SNlist, f.Reslist, f.Deconvolutedlist, f.peaks, f.m_area, f.Analyticalsystem, d.s_Comment FROM  S_Metabolites_Filtered f join S_Metabolome_data d on f.META_Id = d.META_ID WHERE f.Operation_Id = '" + operationId + "'";
            } else {
            }

        } else if (matrixTable.equalsIgnoreCase("S_Metabolite_mining")) {
            querySubTable = "SELECT m.R_Key as 'R_Key', m.META_Id as 'META_Id', m.META_key as 'META_key', m.FeatureNr as 'FeatureNr', m.Retentiontime as 'Retentiontime', m.Charge as 'Charge', m.M1 as 'M1', m.M2, m.M3, m.M4, m.M5, m.mzlist, m.I1, m.I2, m.I3, m.I4, m.I5, m.Intensitylist, m.Rel_Intensitylist, m.Arealist, m.SNlist, m.Reslist, m.Deconvolutedlist, m.peaks, m.m_area, m.Analyticalsystem, d.s_Comment FROM S_Metabolite_mining m join S_Metabolome_data d on m.META_Id = d.META_ID ";

        } else {
            querySubTable = "SELECT R_Key as 'R_Key', META_Id as 'META_Id', META_key as 'META_key', FeatureNr as 'FeatureNr', Retentiontime as 'Retentiontime', Charge as 'Charge', M1 as 'M1', M2, M3, M4, M5, mzlist, I1, I2, I3, I4, I5, Intensitylist, Rel_Intensitylist, Arealist, SNlist, Reslist, Deconvolutedlist, peaks, m_area, Analyticalsystem, Media_type as 's_Comment' FROM  " + matrixTable + " ";
        }

        String[] metaList = metaKeyList.split("\\r?\\n");

        ArrayList<Integer> inputMetaKeyList = new ArrayList<Integer>();
        ArrayList<Double> inputAreaList = new ArrayList<Double>();
        
        for (int j = 0; j < metaList.length; j++) {

            String[] areaSeparate = metaList[j].split(";");

            if (areaSeparate.length != 0) {
                String metaKey = areaSeparate[0];
                inputMetaKeyList.add(Integer.parseInt(metaKey));
                String area = areaSeparate[1];
                inputAreaList.add(Double.parseDouble(area));
            }

        }

        try {
            List<Integer> r_KeyListGroup = new ArrayList<Integer>();
            List<Integer> metaKeyListGroup = new ArrayList<Integer>();
            List<Double> areaListGroup = new ArrayList<Double>();

            for (int j = 0; j < inputMetaKeyList.size(); j++) {

                if (!metaKeyListGroup.contains(inputMetaKeyList.get(j))) {
                    metaKeyListGroup.add(inputMetaKeyList.get(j));
                    areaListGroup.add(inputAreaList.get(j));
                }
            }

            List<MetabolomePackets> metabolomePacketList = new ArrayList<MetabolomePackets>();

            for (int k = 0; k < metaKeyListGroup.size(); k++) {
                String queryMainTableAddition = "";
                queryMainTableAddition = queryMainTable;

                MetabolomePackets metabolomePacketListElement = new MetabolomePackets();
                metabolomePacketListElement.setGroup(metaKeyListGroup.get(k));

                if (mainTable.equalsIgnoreCase("S_Metabolites_Filtered")) {
                    if (operationId != 0) {
                        queryMainTableAddition += " AND f.META_key = '" + metaKeyListGroup.get(k) + "' AND f.m_area >= '" + areaListGroup.get(k) + "' ";
                    } else {
                        queryMainTableAddition += " WHERE f.META_key = '" + metaKeyListGroup.get(k) + "' AND f.m_area >= '" + areaListGroup.get(k) + "' ";
                    }
                } else if (mainTable.equalsIgnoreCase("S_Metabolite_mining")) {
                    queryMainTableAddition += " WHERE m.META_key = '" + metaKeyListGroup.get(k) + "' AND m.m_area >= '" + areaListGroup.get(k) + "' ";
                } else {
                    queryMainTableAddition += " WHERE META_key = '" + metaKeyListGroup.get(k) + "' AND m.m_area >= '" + areaListGroup.get(k) + "' ";
                }

                Statement stateGroup = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                ResultSet resultGroup = stateGroup.executeQuery(queryMainTableAddition);

                List<Metabolome> metabolomeList = new ArrayList<Metabolome>();

                while (resultGroup.next()) {

                    check = true;

                    Metabolome metabolome = new Metabolome();

                    metabolome.setMetaGroup(metaKeyListGroup.get(k));

                    Integer r_Key = resultGroup.getInt("R_Key");
                    metabolome.setR_Key(r_Key);

                    String metaId = resultGroup.getString("META_Id");
                    metabolome.setMetaId(metaId);

                    Integer metaKey = resultGroup.getInt("META_key");
                    metabolome.setMetaKey(metaKey);

                    Integer featureNr = resultGroup.getInt("FeatureNr");
                    metabolome.setFeatureNr(featureNr);

                    Double retTime = resultGroup.getDouble("Retentiontime");
                    metabolome.setRetTime(retTime);

                    Integer charge = resultGroup.getInt("Charge");
                    metabolome.setCharge(charge);

                    Double m1 = resultGroup.getDouble("M1");
                    metabolome.setM1(m1);

                    Double m2 = resultGroup.getDouble("M2");
                    metabolome.setM2(m2);

                    Double m3 = resultGroup.getDouble("M3");
                    metabolome.setM3(m3);

                    Double m4 = resultGroup.getDouble("M4");
                    metabolome.setM4(m4);

                    Double m5 = resultGroup.getDouble("M5");
                    metabolome.setM5(m5);

                    String mzList = resultGroup.getString("mzlist");
                    metabolome.setMzList(mzList);

                    Double i1 = resultGroup.getDouble("I1");
                    metabolome.setI1(i1);

                    Double i2 = resultGroup.getDouble("I2");
                    metabolome.setI2(i2);

                    Double i3 = resultGroup.getDouble("I3");
                    metabolome.setI3(i3);

                    Double i4 = resultGroup.getDouble("I4");
                    metabolome.setI4(i4);

                    Double i5 = resultGroup.getDouble("I5");
                    metabolome.setI5(i5);

                    String intenList = resultGroup.getString("Intensitylist");
                    metabolome.setIntenList(intenList);

                    String Rel_Intensitylist = resultGroup.getString("Rel_Intensitylist");
                    metabolome.setRel_Intensitylist(Rel_Intensitylist);

                    String areaList = resultGroup.getString("Arealist");
                    metabolome.setAreaList(areaList);

                    String snList = resultGroup.getString("SNlist");
                    metabolome.setSnList(snList);

                    String resList = resultGroup.getString("Reslist");
                    metabolome.setResList(resList);

                    String deconvolutedlist = resultGroup.getString("Deconvolutedlist");
                    metabolome.setDeconvolutedlist(deconvolutedlist);

                    Integer peaks = resultGroup.getInt("peaks");
                    metabolome.setPeaks(peaks);

                    Double mArea = resultGroup.getDouble("m_area");
                    metabolome.setmArea(mArea);

                    Integer analyticalSystem = resultGroup.getInt("Analyticalsystem");
                    metabolome.setAnalyticalSystem(analyticalSystem);

                    String sComment = resultGroup.getString("s_Comment");
                    metabolome.setsComment(sComment);

                    metabolomeList.add(metabolome);

                }

                metabolomePacketListElement.setMetabolomeList(metabolomeList);

                metabolomePacketList.add(metabolomePacketListElement);
            }

            Statement stmtSecond = connection.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            ResultSet rsltSecond = stmtSecond.executeQuery(querySubTable);

            List<Double> retTimeListSubTable = new ArrayList<Double>();
            List<Double> m1ListSubTable = new ArrayList<Double>();

            while (rsltSecond.next()) {
                Double retTime = rsltSecond.getDouble("Retentiontime");
                retTimeListSubTable.add(retTime);
                Double m1 = rsltSecond.getDouble("M1");
                m1ListSubTable.add(m1);
            }

            checkCondition(mainUsername, metabolomePacketList, retTimeListSubTable, m1ListSubTable, retTimeRange, massRange, nextOpId, check, connection);
        } catch (SQLException ex) {

        }

    }

    private void checkCondition(String mainUsername, List<MetabolomePackets> metabolomePacketList, List<Double> retTimeListSubTable, List<Double> m1ListSubTable, Double retTimeRange, Double massRange, int nextOpId, boolean check, Connection connection) {
        int i = 0;
        for (int m = 0; m < metabolomePacketList.size(); m++) {

            Iterator<Metabolome> it = metabolomePacketList.get(m).getMetabolomeList().iterator();
            while (it.hasNext()) {
                Metabolome meta = it.next();

                for (int j = 0; j < retTimeListSubTable.size(); j++) {

                    Double retRangeMin = retTimeListSubTable.get(j) - retTimeRange;
                    Double massRangeMin = m1ListSubTable.get(j) - massRange;
                    Double retRangeMax = retTimeListSubTable.get(j) + retTimeRange;
                    Double massRangeMax = m1ListSubTable.get(j) + massRange;


                    Double mass = meta.getM1();
                    Double ret = meta.getRetTime();

                    if ((ret >= retRangeMin) && (ret <= retRangeMax) && (mass >= massRangeMin) && (mass <= massRangeMax)) {
                        i++;
                        it.remove();
                        break;
                    }
                }
            }
        }

        sortMetabolomes(mainUsername, metabolomePacketList, nextOpId, check, connection);
    }

    private void sortMetabolomes(String mainUsername, List<MetabolomePackets> metabolomePacket, int nextOpId, boolean check, Connection connection) {
        List<MetabolomePackets> metabolomePacketListSorted = new ArrayList<MetabolomePackets>();


        for (int k = 0; k < metabolomePacket.size(); k++) {
            MetabolomePackets metabolomePacketListElementSorted = new MetabolomePackets();
            metabolomePacketListElementSorted.setGroup(metabolomePacket.get(k).getGroup());

            List<Double> areaList = new ArrayList<Double>();
            List<Integer> rkeyList = new ArrayList<Integer>();
            List<Integer> groupList = new ArrayList<Integer>();
            List<String> metaidList = new ArrayList<String>();
            List<Integer> metakeyList = new ArrayList<Integer>();
            List<Integer> analyticalsystemList = new ArrayList<Integer>();
            List<String> arealistList = new ArrayList<String>();
            List<Integer> chargeList = new ArrayList<Integer>();
            List<String> deconvolutedlistList = new ArrayList<String>();
            List<Integer> featurenrList = new ArrayList<Integer>();
            List<Double> i1List = new ArrayList<Double>();
            List<Double> i2List = new ArrayList<Double>();
            List<Double> i3List = new ArrayList<Double>();
            List<Double> i4List = new ArrayList<Double>();
            List<Double> i5List = new ArrayList<Double>();
            List<String> intenlistList = new ArrayList<String>();
            List<Double> m1List = new ArrayList<Double>();
            List<Double> m2List = new ArrayList<Double>();
            List<Double> m3List = new ArrayList<Double>();
            List<Double> m4List = new ArrayList<Double>();
            List<Double> m5List = new ArrayList<Double>();
            List<String> mzlistList = new ArrayList<String>();
            List<Integer> peaksList = new ArrayList<Integer>();
            List<String> rel_intensitylistList = new ArrayList<String>();
            List<String> reslistList = new ArrayList<String>();
            List<Double> rettimeList = new ArrayList<Double>();
            List<String> snlistList = new ArrayList<String>();
            List<String> scommentList = new ArrayList<String>();

            HashMap<Integer, Double> rKeymArea = new HashMap<Integer, Double>();
            TreeMap<Integer, Double> sortedMap = new TreeMap<Integer, Double>();

            Iterator<Metabolome> iterator = metabolomePacket.get(k).getMetabolomeList().iterator();
            Metabolome meta = new Metabolome();
            while (iterator.hasNext()) {

                meta = iterator.next();

                Double mArea = meta.getmArea();
                areaList.add(mArea);

                int rKey = meta.getR_Key();
                rkeyList.add(rKey);

                rKeymArea.put(rKey, mArea);

                int group = meta.getMetaGroup();
                groupList.add(group);

                String metaid = meta.getMetaId();
                metaidList.add(metaid);

                int metakey = meta.getMetaKey();
                metakeyList.add(metakey);

                int analyticalsystem = meta.getAnalyticalSystem();
                analyticalsystemList.add(analyticalsystem);

                String arealist = meta.getAreaList();
                arealistList.add(arealist);

                int charge = meta.getCharge();
                chargeList.add(charge);

                String deconvolutedlist = meta.getDeconvolutedlist();
                deconvolutedlistList.add(deconvolutedlist);

                int featurenr = meta.getFeatureNr();
                featurenrList.add(featurenr);

                double i1 = meta.getI1();
                i1List.add(i1);
                double i2 = meta.getI2();
                i2List.add(i2);
                double i3 = meta.getI3();
                i3List.add(i3);
                double i4 = meta.getI4();
                i4List.add(i4);
                double i5 = meta.getI5();
                i5List.add(i5);

                String intenlist = meta.getIntenList();
                intenlistList.add(intenlist);

                double m1 = meta.getM1();
                m1List.add(m1);
                double m2 = meta.getM2();
                m2List.add(m2);
                double m3 = meta.getM3();
                m3List.add(m3);
                double m4 = meta.getM4();
                m4List.add(m4);
                double m5 = meta.getM5();
                m5List.add(m5);

                String mzlist = meta.getMzList();
                mzlistList.add(mzlist);
                int peaks = meta.getPeaks();
                peaksList.add(peaks);
                String rel_intensitylist = meta.getRel_Intensitylist();
                rel_intensitylistList.add(rel_intensitylist);
                String reslist = meta.getResList();
                reslistList.add(reslist);
                double rettime = meta.getRetTime();
                rettimeList.add(rettime);
                String snlist = meta.getSnList();
                snlistList.add(snlist);
                String scomment = meta.getsComment();
                scommentList.add(scomment);

            }
            ValueComparator bvc = new ValueComparator(rKeymArea);
            sortedMap = new TreeMap<Integer, Double>(bvc);
            sortedMap.putAll(rKeymArea);
    
            List<Metabolome> metabolomeListSorted = new ArrayList<Metabolome>();

            for (int key : sortedMap.keySet()) {
                for (int m = 0; m < rkeyList.size(); m++) {
                    if (key == rkeyList.get(m)) {

                        Metabolome metabolomeSorted = new Metabolome();

                        metabolomeSorted.setMetaGroup(groupList.get(m));
                        metabolomeSorted.setAnalyticalSystem(analyticalsystemList.get(m));
                        metabolomeSorted.setAreaList(arealistList.get(m));
                        metabolomeSorted.setCharge(chargeList.get(m));
                        metabolomeSorted.setDeconvolutedlist(deconvolutedlistList.get(m));
                        metabolomeSorted.setFeatureNr(featurenrList.get(m));
                        metabolomeSorted.setI1(i1List.get(m));
                        metabolomeSorted.setI2(i2List.get(m));
                        metabolomeSorted.setI3(i3List.get(m));
                        metabolomeSorted.setI4(i4List.get(m));
                        metabolomeSorted.setI5(i5List.get(m));
                        metabolomeSorted.setIntenList(intenlistList.get(m));
                        metabolomeSorted.setM1(m1List.get(m));
                        metabolomeSorted.setM2(m2List.get(m));
                        metabolomeSorted.setM3(m3List.get(m));
                        metabolomeSorted.setM4(m4List.get(m));
                        metabolomeSorted.setM5(m5List.get(m));
                        metabolomeSorted.setMetaId(metaidList.get(m));
                        metabolomeSorted.setMetaKey(metakeyList.get(m));
                        metabolomeSorted.setMzList(mzlistList.get(m));
                        metabolomeSorted.setPeaks(peaksList.get(m));
                        metabolomeSorted.setR_Key(rkeyList.get(m));
                        metabolomeSorted.setRel_Intensitylist(rel_intensitylistList.get(m));
                        metabolomeSorted.setResList(reslistList.get(m));
                        metabolomeSorted.setRetTime(rettimeList.get(m));
                        metabolomeSorted.setSnList(snlistList.get(m));
                        metabolomeSorted.setmArea(areaList.get(m));
                        metabolomeSorted.setsComment(scommentList.get(m));

                        metabolomeListSorted.add(metabolomeSorted);

                    }
                }
            }
            metabolomePacketListElementSorted.setMetabolomeList(metabolomeListSorted);
            metabolomePacketListSorted.add(metabolomePacketListElementSorted);
        }

    
        insertGroupedTable(mainUsername, metabolomePacketListSorted, nextOpId, check, connection);

    }

    public void insertGroupedTable(String mainUsername, List<MetabolomePackets> metabolomePacketListSorted, int nextOpId, boolean check, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        for (int m = 0; m < metabolomePacketListSorted.size(); m++) {

            int groupNo = metabolomePacketListSorted.get(m).getGroup();

            for (int n = 0; n < metabolomePacketListSorted.get(m).getMetabolomeList().size(); n++) {

                Metabolome meta = new Metabolome();
                meta = metabolomePacketListSorted.get(m).getMetabolomeList().get(n);

                String insertQuery = "INSERT INTO S_Metabolites_Filtered VALUES(Null, '" + meta.getR_Key() + "','" + meta.getMetaId() + "','" + meta.getMetaKey() + "','" + meta.getFeatureNr() + "','" + meta.getRetTime() + "','" + meta.getCharge() + "','" + meta.getM1() + "','" + meta.getM2() + "','" + meta.getM3() + "','" + meta.getM4() + "','" + meta.getM5() + "','" + meta.getMzList() + "','" + meta.getI1() + "','" + meta.getI2() + "','" + meta.getI3() + "','" + meta.getI4() + "','" + meta.getI5() + "','" + meta.getIntenList() + "','" + meta.getRel_Intensitylist() + "','" + meta.getAreaList() + "','" + meta.getSnList() + "','" + meta.getResList() + "','" + meta.getDeconvolutedlist() + "','" + meta.getPeaks() + "','" + meta.getmArea() + "','" + meta.getAnalyticalSystem() + "','" + meta.getsComment() + "','" + nextOpId + "','" + mainUsername + "','" + dateFormat.format(date) + "')";

                try {
                    Statement st = (Statement) connection.createStatement();
                    st.executeUpdate(insertQuery);
                } catch (SQLException ex) {
                }
                /*
                if (check) {
                    insertIntoMetabolomeResult(mainUsername, meta.getMetaId(), "It returns results!", nextOpId, connection);
                } else {
                    insertIntoMetabolomeResult(mainUsername, "Null", "It does not return any results!", nextOpId, connection);
                }
                */
            }
        }

    }

    public void insertIntoMetabolomeResult(String mainUsername, String META_Id, String Details, int opId, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");

        Date date = new Date();
        String insertQuery = "";

        insertQuery = "INSERT INTO S_Metabolite_Results VALUES(Null, 'Matrix Filtering','" + META_Id + "','" + opId + "','" + Details + "','" + mainUsername + "', '" + dateFormat.format(date) + "' )";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(insertQuery);
        } catch (SQLException ex) {
        }
    }

}
