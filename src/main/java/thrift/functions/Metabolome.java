/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

/**
 *
 * @author nibo01
 */
public class Metabolome {

    public int r_Key = 0;
    public String metaId = "";
    public int metaKey = 0;
    public int featureNr = 0;
    public double retTime = 0.0;
    public int charge = 0;
    public double M1 = 0.0;
    public double M2 = 0.0;
    public double M3 = 0.0;
    public double M4 = 0.0;
    public double M5 = 0.0;
    public String mzList = "";
    public double i1 = 0.0;
    public double i2 = 0.0;
    public double i3 = 0.0;
    public double i4 = 0.0;
    public double i5 = 0.0;
    public String intenList = "";
    public String rel_Intensitylist = "";
    public String areaList = "";
    public String snList = "";
    public String resList = "";
    public String deconvolutedlist = "";
    public int peaks = 0;
    public double mArea = 0.0;
    public int analyticalSystem = 0;
    public String sComment = "";
    
    public int metaGroup = 0;

    public int getR_Key() {
        return r_Key;
    }

    public void setR_Key(int r_Key) {
        this.r_Key = r_Key;
    }

    public String getMetaId() {
        return metaId;
    }

    public void setMetaId(String metaId) {
        this.metaId = metaId;
    }

    public int getMetaKey() {
        return metaKey;
    }

    public void setMetaKey(int metaKey) {
        this.metaKey = metaKey;
    }

    public int getFeatureNr() {
        return featureNr;
    }

    public void setFeatureNr(int featureNr) {
        this.featureNr = featureNr;
    }

    public double getRetTime() {
        return retTime;
    }

    public void setRetTime(double retTime) {
        this.retTime = retTime;
    }

    public int getCharge() {
        return charge;
    }

    public void setCharge(int charge) {
        this.charge = charge;
    }

    public double getM1() {
        return M1;
    }

    public void setM1(double M1) {
        this.M1 = M1;
    }

    public double getM2() {
        return M2;
    }

    public void setM2(double M2) {
        this.M2 = M2;
    }

    public double getM3() {
        return M3;
    }

    public void setM3(double M3) {
        this.M3 = M3;
    }

    public double getM4() {
        return M4;
    }

    public void setM4(double M4) {
        this.M4 = M4;
    }

    public double getM5() {
        return M5;
    }

    public void setM5(double M5) {
        this.M5 = M5;
    }

    public String getMzList() {
        return mzList;
    }

    public void setMzList(String mzList) {
        this.mzList = mzList;
    }

    public double getI1() {
        return i1;
    }

    public void setI1(double i1) {
        this.i1 = i1;
    }

    public double getI2() {
        return i2;
    }

    public void setI2(double i2) {
        this.i2 = i2;
    }

    public double getI3() {
        return i3;
    }

    public void setI3(double i3) {
        this.i3 = i3;
    }

    public double getI4() {
        return i4;
    }

    public void setI4(double i4) {
        this.i4 = i4;
    }

    public double getI5() {
        return i5;
    }

    public void setI5(double i5) {
        this.i5 = i5;
    }

    public String getIntenList() {
        return intenList;
    }

    public void setIntenList(String intenList) {
        this.intenList = intenList;
    }

    public String getRel_Intensitylist() {
        return rel_Intensitylist;
    }

    public void setRel_Intensitylist(String rel_Intensitylist) {
        this.rel_Intensitylist = rel_Intensitylist;
    }

    public String getAreaList() {
        return areaList;
    }

    public void setAreaList(String areaList) {
        this.areaList = areaList;
    }

    public String getSnList() {
        return snList;
    }

    public void setSnList(String snList) {
        this.snList = snList;
    }

    public String getResList() {
        return resList;
    }

    public void setResList(String resList) {
        this.resList = resList;
    }

    public String getDeconvolutedlist() {
        return deconvolutedlist;
    }

    public void setDeconvolutedlist(String deconvolutedlist) {
        this.deconvolutedlist = deconvolutedlist;
    }

    public int getPeaks() {
        return peaks;
    }

    public void setPeaks(int peaks) {
        this.peaks = peaks;
    }

    public double getmArea() {
        return mArea;
    }

    public void setmArea(double mArea) {
        this.mArea = mArea;
    }

    public int getAnalyticalSystem() {
        return analyticalSystem;
    }

    public void setAnalyticalSystem(int analyticalSystem) {
        this.analyticalSystem = analyticalSystem;
    }

    public String getsComment() {
        return sComment;
    }

    public void setsComment(String sComment) {
        this.sComment = sComment;
    }

    public int getMetaGroup() {
        return metaGroup;
    }

    public void setMetaGroup(int metaGroup) {
        this.metaGroup = metaGroup;
    }

}
