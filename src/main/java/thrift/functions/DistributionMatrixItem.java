/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

/**
 *
 * @author nibo01
 */
public class DistributionMatrixItem {

    public String sDomain;
    public String sKingdom;
    public String sPhylum;
    public String sClass;
    public String sOrder;
    public String sSuborder;
    public String sFamily;
    public String sGenus;
    public String sSpecies;
    public String sSubSpecies;
    public String sStrainName;
    public String sStrainKey;

    public String getsDomain() {
        return sDomain;
    }

    public void setsDomain(String sDomain) {
        this.sDomain = sDomain;
    }

    public String getsKingdom() {
        return sKingdom;
    }

    public void setsKingdom(String sKingdom) {
        this.sKingdom = sKingdom;
    }

    public String getsPhylum() {
        return sPhylum;
    }

    public void setsPhylum(String sPhylum) {
        this.sPhylum = sPhylum;
    }

    public String getsClass() {
        return sClass;
    }

    public void setsClass(String sClass) {
        this.sClass = sClass;
    }

    public String getsOrder() {
        return sOrder;
    }

    public void setsOrder(String sOrder) {
        this.sOrder = sOrder;
    }

    public String getsSuborder() {
        return sSuborder;
    }

    public void setsSuborder(String sSuborder) {
        this.sSuborder = sSuborder;
    }

    public String getsFamily() {
        return sFamily;
    }

    public void setsFamily(String sFamily) {
        this.sFamily = sFamily;
    }

    public String getsGenus() {
        return sGenus;
    }

    public void setsGenus(String sGenus) {
        this.sGenus = sGenus;
    }

    public String getsSpecies() {
        return sSpecies;
    }

    public void setsSpecies(String sSpecies) {
        this.sSpecies = sSpecies;
    }

    public String getsSubSpecies() {
        return sSubSpecies;
    }

    public void setsSubSpecies(String sSubSpecies) {
        this.sSubSpecies = sSubSpecies;
    }

    public String getsStrainName() {
        return sStrainName;
    }

    public void setsStrainName(String sStrainName) {
        this.sStrainName = sStrainName;
    }

    public String getsStrainKey() {
        return sStrainKey;
    }

    public void setsStrainKey(String sStrainKey) {
        this.sStrainKey = sStrainKey;
    }
    
    

}
