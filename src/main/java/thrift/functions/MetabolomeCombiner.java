/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author nibo01
 */
public class MetabolomeCombiner {

    List<MetabolomeBuckets> metabolomeBuckets = new ArrayList<MetabolomeBuckets>();
    
    String filePath ="";

    void metabolomeCombineMETA(String mainUsername, List<Integer> rKey, List<String> metaId, List<Integer> mKey, List<Double> retTime, List<Double> M1, List<Double> mArea, int minRKey, int maxRKey, int operationId, String tableName, int nextOpId, Double massRange, Double retTimeRange, Connection conn) {

        long startTime = System.currentTimeMillis();

        int bucketNo = 1;
        for (int i = 0; i < rKey.size(); i++) {

            if (metabolomeBuckets.isEmpty()) {
                int countMetaEmpty = 1;

                MetabolomeBuckets bucket = new MetabolomeBuckets();
                List<Object> metaDetails = new ArrayList<Object>();
                List<List<Object>> details = new ArrayList<List<Object>>();
                metaDetails.add(rKey.get(i));
                metaDetails.add(metaId.get(i));
                metaDetails.add(mKey.get(i));
                metaDetails.add(retTime.get(i));
                metaDetails.add(M1.get(i));
                metaDetails.add(mArea.get(i));
                metaDetails.add(bucketNo);
                //metaDetails.add(retTime.get(i));
                //metaDetails.add(M1.get(i));

                details.add(metaDetails);

                bucket.setAverageMass(M1.get(i));
                bucket.setAverageRetTime(retTime.get(i));
                bucket.setDetails(details);
                bucket.setBucketNo(bucketNo);
                bucket.setMetaCount(countMetaEmpty);
                metabolomeBuckets.add(bucket);

            } else {

                boolean checkIf = false;
                int m = 0;
                while (m <= metabolomeBuckets.size()) {

                    List<Integer> rKeyCheckedIf = new ArrayList<Integer>();
                    List<Integer> rKeyNotCheckedIf = new ArrayList<Integer>();


                    try {
                        if ((M1.get(i) >= metabolomeBuckets.get(m).getAverageMass() - massRange) && (M1.get(i) <= metabolomeBuckets.get(m).getAverageMass() + massRange) && (retTime.get(i) >= metabolomeBuckets.get(m).getAverageRetTime() - retTimeRange) && (retTime.get(i) <= metabolomeBuckets.get(m).getAverageRetTime() + retTimeRange)) {
                            int countMetaNotExisted = metabolomeBuckets.get(m).getDetails().size();
                            countMetaNotExisted++;

                            rKeyCheckedIf.add(rKey.get(i));

                            checkIf = true;

                            List<Object> metaDetails = new ArrayList<Object>();
                            List<List<Object>> details = metabolomeBuckets.get(m).getDetails();
                            metaDetails.add(rKey.get(i));
                            metaDetails.add(metaId.get(i));
                            metaDetails.add(mKey.get(i));
                            metaDetails.add(retTime.get(i));
                            metaDetails.add(M1.get(i));
                            metaDetails.add(mArea.get(i));
                            metaDetails.add(metabolomeBuckets.get(m).getBucketNo());

                            List<Double> mass = new ArrayList<Double>();
                            mass.add(M1.get(i));
                            mass.add(metabolomeBuckets.get(m).getAverageMass());

                            List<Double> ret = new ArrayList<Double>();
                            ret.add(retTime.get(i));
                            ret.add(metabolomeBuckets.get(m).getAverageRetTime());

                            Double massAvg = calculateAverage(mass);
                            Double retAvg = calculateAverage(ret);

                            //metaDetails.add(retAvg);
                            //metaDetails.add(massAvg);
                            details.add(metaDetails);

                            metabolomeBuckets.get(m).setAverageMass(massAvg);
                            metabolomeBuckets.get(m).setAverageRetTime(retAvg);
                            metabolomeBuckets.get(m).setDetails(details);
                            metabolomeBuckets.get(m).setMetaCount(countMetaNotExisted);
                            //continue;
                        }

                    } catch (Exception ex) {
                        //ex.printStackTrace();
                    }

                    m++;
                    if (m == metabolomeBuckets.size()) {
                        if (checkIf == false) {
                            int countMetaCreate = 1;
                            bucketNo++;

                            MetabolomeBuckets bucket = new MetabolomeBuckets();
                            List<Object> metaDetails = new ArrayList<Object>();
                            List<List<Object>> details = new ArrayList<List<Object>>();
                            metaDetails.add(rKey.get(i));
                            metaDetails.add(metaId.get(i));
                            metaDetails.add(mKey.get(i));
                            metaDetails.add(retTime.get(i));
                            metaDetails.add(M1.get(i));
                            metaDetails.add(mArea.get(i));
                            metaDetails.add(bucketNo);
                            //metaDetails.add(retTime.get(i));
                            //metaDetails.add(M1.get(i));

                            details.add(metaDetails);

                            bucket.setAverageMass(M1.get(i));
                            bucket.setAverageRetTime(retTime.get(i));
                            bucket.setBucketNo(bucketNo);
                            bucket.setDetails(details);
                            bucket.setMetaCount(countMetaCreate);

                            metabolomeBuckets.add(bucket);

                            break;
                        }
                    }
                }
            }
        }

        for (int k = 0; k < metabolomeBuckets.size(); k++) {

            List<Double> mass = new ArrayList<Double>();
            List<Double> ret = new ArrayList<Double>();

            for (int m = 0; m < metabolomeBuckets.get(k).getDetails().size(); m++) {

                ret.add(Double.parseDouble(metabolomeBuckets.get(k).getDetails().get(m).get(3).toString()));
                mass.add(Double.parseDouble(metabolomeBuckets.get(k).getDetails().get(m).get(4).toString()));
            }
            Double retAv = calculateAverage(ret);
            Double massAv = calculateAverage(mass);
            metabolomeBuckets.get(k).setAverageRetTime(retAv);
            metabolomeBuckets.get(k).setAverageMass(massAv);
        }

        for (int k = 0; k < metabolomeBuckets.size(); k++) {

            for (int m = 0; m < metabolomeBuckets.get(k).getDetails().size(); m++) {

                List<Object> details = metabolomeBuckets.get(k).getDetails().get(m);

                insertIntoMetabolomeBucket(mainUsername, details.get(0).toString(), details.get(1).toString(), details.get(2).toString(), details.get(3).toString(), details.get(4).toString(), details.get(5).toString(), details.get(6).toString(), metabolomeBuckets.get(k).getAverageRetTime().toString(), metabolomeBuckets.get(k).getAverageMass().toString(), nextOpId, conn);
                //generateCsvFile(filePath, metabolomeBuckets);
            }

        }

        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;

    }

    //Combine with query
    void metabolomeCombine(List<Integer> rKey, List<Integer> mKey, List<Double> retTime, List<Double> M1, List<Double> mArea, int minRKey, int maxRKey, int operationId, int folds, String tableName, int opId, String filePath, Connection conn) {

        long startTime = System.currentTimeMillis();

        int bucketNo = 1;

        for (int j = 0; j < rKey.size(); j++) {

            Double minMass = M1.get(j) - 0.02;
            Double maxMass = M1.get(j) + 0.02;
            Double minRet = retTime.get(j) - 0.2;
            Double maxRet = retTime.get(j) + 0.2;

            try {
                Statement stmt = conn.createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
                String selectQuery = "";
                if (tableName.equals("S_Metabolite_mining")) {
                    selectQuery = "select R_Key, META_key, Retentiontime, M1, m_area from S_Metabolite_mining where M1 between '" + minMass + "' and '" + maxMass + "' and Retentiontime between '" + minRet + "' and '" + maxRet + "' and R_Key between '" + minRKey + "' and '" + maxRKey + "' ";
                } else if (tableName.equals("S_Metabolites_Filtered")) {
                    selectQuery = "select R_Key, META_key, Retentiontime, M1, m_area from S_Metabolites_Filtered where M1 between '" + minMass + "' and '" + maxMass + "' and Retentiontime between '" + minRet + "' and '" + maxRet + "' and Operation_Id = '" + operationId + "' ";
                } else {
                    selectQuery = "select R_Key, META_key, Retentiontime, M1, m_area from " + tableName + " where M1 between '" + minMass + "' and '" + maxMass + "' and Retentiontime between '" + minRet + "' and '" + maxRet + "' ";
                }

                ResultSet rslt = stmt.executeQuery(selectQuery);

                int count = 0;

                List<Integer> rKeyList = new ArrayList<Integer>();
                List<Integer> mKeyList = new ArrayList<Integer>();
                List<Double> retTimeList = new ArrayList<Double>();
                List<Double> M1List = new ArrayList<Double>();
                List<Double> mAreaList = new ArrayList<Double>();

                while (rslt.next()) {

                    count++;

                    int rKeyQuery = rslt.getInt("R_Key");
                    int mKeyQuery = rslt.getInt("META_key");
                    double retTimeQuery = rslt.getDouble("Retentiontime");
                    double M1Query = rslt.getDouble("M1");
                    double mAreaQuery = rslt.getDouble("m_area");


                    rKeyList.add(rKeyQuery);
                    mKeyList.add(mKeyQuery);
                    retTimeList.add(retTimeQuery);
                    M1List.add(M1Query);
                    mAreaList.add(mAreaQuery);

                }

                if (metabolomeBuckets.isEmpty()) {
                } else {
                    for (int i = 0; i < metabolomeBuckets.size(); i++) {
                        List<List<Object>> combiListFinal = metabolomeBuckets.get(i).getDetails();
                        for (int m = 0; m < metabolomeBuckets.get(i).getDetails().size(); m++) {
                            List<Object> bucketDetails = metabolomeBuckets.get(i).getDetails().get(m);
                            //String[] bucket = bucketDetails.split(";");
                            int RKEY = Integer.parseInt(bucketDetails.get(0).toString());
                            int BUCKETNO = Integer.parseInt(bucketDetails.get(5).toString());

                            for (int k = 0; k < rKeyList.size(); k++) {
                                if (rKeyList.contains(RKEY)) {

                                    rKeyList.remove(k);
                                    mKeyList.remove(k);
                                    retTimeList.remove(k);
                                    M1List.remove(k);
                                    mAreaList.remove(k);
                                    //break;
                                    /*

                                     Double combineBucketRET = metabolomeBuckets.get(i).getAverageRetTime();
                                     Double combineBucketMASS = metabolomeBuckets.get(i).getAverageMass();
                                    
                                     int finalBucket = metabolomeBuckets.get(i).getBucketNo();

                                     Double combineRET = calculateAverage(retTimeList);
                                     Double combineMASS = calculateAverage(M1List);

                                     List<Double> combineListRET = new ArrayList<Double>();
                                     combineListRET.add(combineBucketRET);
                                     combineListRET.add(combineRET);
                                     Double finalRET = calculateAverage(combineListRET);

                                     List<Double> combineListMASS = new ArrayList<Double>();
                                     combineListMASS.add(combineBucketMASS);
                                     combineListMASS.add(combineMASS);
                                     Double finalMASS = calculateAverage(combineListMASS);

                                     metabolomeBuckets.get(i).setAverageRetTime(finalRET);
                                     metabolomeBuckets.get(i).setAverageMass(finalMASS);

                                     for (int g = 0; g < rKeyList.size(); g++) {
                                     List<Object> finalBucketDetails = new ArrayList<Object>();

                                     finalBucketDetails.add(rKeyList.get(k));
                                     finalBucketDetails.add(mKeyList.get(k));
                                     finalBucketDetails.add(retTimeList.get(k));
                                     finalBucketDetails.add(M1List.get(k));
                                     finalBucketDetails.add(mAreaList.get(k));
                                     finalBucketDetails.add(finalBucket);
                                     finalBucketDetails.add(finalRET);
                                     finalBucketDetails.add(finalMASS);

                                     combiListFinal.add(finalBucketDetails);
                                     }

                                     metabolomeBuckets.get(i).setDetails(combiListFinal);
                                     */
                                } else {
                                    break;
                                }
                            }
                        }

                    }
                }

                ArrayList<List<Object>> combiList = new ArrayList<List<Object>>();

                if ((count > 1) && (!rKeyList.isEmpty())) {

                    MetabolomeBuckets metaBucket = new MetabolomeBuckets();

                    Double averageCalculateRet = calculateAverage(retTimeList);
                    metaBucket.setAverageRetTime(averageCalculateRet);
                    Double averageCalculateMass = calculateAverage(M1List);
                    metaBucket.setAverageMass(averageCalculateMass);
                    metaBucket.setBucketNo(bucketNo);

                    for (int k = 0; k < rKeyList.size(); k++) {

                        List<Object> bucketDetails = new ArrayList<Object>();
                        bucketDetails.add(rKeyList.get(k));
                        bucketDetails.add(mKeyList.get(k));
                        bucketDetails.add(retTimeList.get(k));
                        bucketDetails.add(M1List.get(k));
                        bucketDetails.add(mAreaList.get(k));
                        bucketDetails.add(bucketNo);
                        bucketDetails.add(averageCalculateRet);
                        bucketDetails.add(averageCalculateMass);

                        combiList.add(bucketDetails);
                    }
                    metaBucket.setDetails(combiList);

                    metabolomeBuckets.add(metaBucket);

                    bucketNo++;

                    ///bucketTemp.put(bucketNo, combiList);
                }

            } catch (SQLException ex) {

            }

        }

        for (int k = 0; k < metabolomeBuckets.size(); k++) {
            for (int m = 0; m < metabolomeBuckets.get(k).getDetails().size(); m++) {
                //String details = bucketTemp.get(k).get(m);
                //String[] detailsSeparate = details.split(";");

                /*
                 List<Object> details = metabolomeBuckets.get(k).getDetails().get(m);
                 insertIntoMetabolomeBucket(details.get(0).toString(), details.get(1).toString(), details.get(2).toString(), details.get(3).toString(), details.get(4).toString(), details.get(5).toString(), details.get(6).toString(), details.get(7).toString(), opId, conn);
                 generateCsvFile(filePath, metabolomeBuckets);
                 */
            }
        }
        int count = 0;


        long endTime = System.currentTimeMillis();
        long totalTime = endTime - startTime;
    }

    private double calculateAverage(List<Double> list) {

        double sum = 0;

        for (int i = 0; i < list.size(); i++) {
            sum += Double.valueOf(list.get(i));
        }
        double average = sum / list.size();

        return average;
    }

    //Insert values into buckets
    //private void insertIntoMetabolomeBucket(Integer rKey, Integer mKey, Double retTime, Double M1, Double mArea, int bucketNo, int threshold, Connection conn) {
    public void insertIntoMetabolomeBucket(String mainUsername, String rKey, String metaId, String mKey, String retTime, String M1, String mArea, String bucketNo, String avRetTime, String avMass, int opId, Connection conn) {

        boolean check = false;
        if (!rKey.equals("")) {
            check = true;
        }

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String insertQuery = "INSERT INTO S_Metabolite_Buckets VALUES(Null, '" + rKey + "', '" + metaId + "', '" + mKey + "', '" + retTime + "', '" + M1 + "', '" + mArea + "', '" + bucketNo + "', '" + avRetTime + "', '" + avMass + "', '" + opId + "','" + mainUsername + "','" + dateFormat.format(date) + "' )";

        try {
            Statement st = (Statement) conn.createStatement();
            st.executeUpdate(insertQuery);
        } catch (SQLException ex) {
            ex.printStackTrace();
        }

        /*
        if (check) {
            insertIntoMetabolomeResult(mainUsername, metaId, "It returns results!", opId, conn);
        } else {
            insertIntoMetabolomeResult(mainUsername, "Null", "It does not return any results!", opId, conn);
        }
        */
    }

    public void insertIntoMetabolomeResult(String mainUsername, String metaId, String Details, int opId, Connection connection) {

        DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();

        String insertQuery = "";

        insertQuery = "INSERT INTO S_Metabolite_Results VALUES(Null, 'Bucketing','" + metaId + "','" + opId + "','" + Details + "','" + mainUsername + "', '" + dateFormat.format(date) + "' )";

        try {
            Statement st = (Statement) connection.createStatement();
            st.executeUpdate(insertQuery);
        } catch (SQLException ex) {
        }

    }

    private void generateCsvFile(String filePath, List<MetabolomeBuckets> metabolomeBuckets) {

        try {
            FileWriter writer = new FileWriter(filePath);
            writer.append("Bucket_No");
            writer.append(';');
            writer.append("Average_RT");
            writer.append(';');
            writer.append("Average_Mass");
            writer.append(';');
            writer.append("R_Key");
            writer.append(';');
            writer.append("M_Key");
            writer.append('\n');
            /*
             for (int i = 0; i < metabolomeBuckets.size(); i++) {
             for (int k = 0; k < metabolomeBuckets.get(i).getDetails().size(); k++) {

             writer.append("RKEY " + k);
             writer.append(',');
             writer.append("MKEY " + k);
             writer.append('\n');

             }
             }
             */

            //for (int i = 0; i < metabolomeBuckets.size(); i++) {
            //  writer.append(Integer.toString(metabolomeBuckets.get(i).getBucketNo()));
            // writer.append(Double.toHexString(metabolomeBuckets.get(i).getAverageRetTime()));
            // writer.append(Double.toHexString(metabolomeBuckets.get(i).getAverageMass()));

            /*
             for (int k = 0; k < metabolomeBuckets.get(i).getDetails().size(); k++) {
             List<Object> list = metabolomeBuckets.get(i).getDetails().get(k);

             writer.append(metabolomeBuckets.get(i).getDetails().get(k).get(1).toString());
             writer.append(',');
             writer.append(metabolomeBuckets.get(i).getDetails().get(k).get(1).toString());
             writer.append('\n');

             }
             */
            //}
            for (int j = 0; j < metabolomeBuckets.size(); j++) {
                writer.append(Integer.toString(metabolomeBuckets.get(j).getBucketNo()));
                writer.append(';');
                writer.append(Double.toString(metabolomeBuckets.get(j).getAverageRetTime()));
                writer.append(';');
                writer.append(Double.toString(metabolomeBuckets.get(j).getAverageMass()));
                writer.append(';');
                for (int m = 0; m < metabolomeBuckets.get(j).getDetails().size(); m++) {
                    writer.append(metabolomeBuckets.get(j).getDetails().get(m).get(0).toString());
                    writer.append(',');
                }
                writer.append(';');
                for (int n = 0; n < metabolomeBuckets.get(j).getDetails().size(); n++) {
                    writer.append(metabolomeBuckets.get(j).getDetails().get(n).get(1).toString());
                    writer.append(',');
                }

                writer.append('\n');
            }

            writer.flush();
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();

        }

    }
}
