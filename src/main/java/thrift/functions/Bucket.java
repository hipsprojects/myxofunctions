/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package thrift.functions;

import java.util.List;

/**
 *
 * @author nibo01
 */
public class Bucket {
    
    public int bucketNo = 0;
    List<Integer> metaKeyCollection;
    List<Double> mAreaCollection;
    List<Integer> mAreaCount;

    public int getBucketNo() {
        return bucketNo;
    }

    public void setBucketNo(int bucketNo) {
        this.bucketNo = bucketNo;
    }

    public List<Integer> getMetaKeyCollection() {
        return metaKeyCollection;
    }

    public void setMetaKeyCollection(List<Integer> metaKeyCollection) {
        this.metaKeyCollection = metaKeyCollection;
    }
  
    public List<Double> getmAreaCollection() {
        return mAreaCollection;
    }

    public void setmAreaCollection(List<Double> mAreaCollection) {
        this.mAreaCollection = mAreaCollection;
    }

    public List<Integer> getmAreaCount() {
        return mAreaCount;
    }

    public void setmAreaCount(List<Integer> mAreaCount) {
        this.mAreaCount = mAreaCount;
    }
    
    
}
