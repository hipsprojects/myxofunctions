/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

import java.util.List;

/**
 *
 * @author nibo01
 */
public class MetabolomeBuckets {

    public int bucketNo = 0;
    public List<List<Object>> details;
    public Double averageRetTime = 0.0;
    public Double averageMass = 0.0;
    public int metaCount = 0; 
    
    public int getBucketNo() {
        return bucketNo;
    }

    public void setBucketNo(int bucketNo) {
        this.bucketNo = bucketNo;
    }

    public List<List<Object>> getDetails() {
        return details;
    }

    public void setDetails(List<List<Object>> details) {
        this.details = details;
    }

    public Double getAverageRetTime() {
        return averageRetTime;
    }

    public void setAverageRetTime(Double averageRetTime) {
        this.averageRetTime = averageRetTime;
    }

    public Double getAverageMass() {
        return averageMass;
    }

    public void setAverageMass(Double averageMass) {
        this.averageMass = averageMass;
    }

    public int getMetaCount() {
        return metaCount;
    }

    public void setMetaCount(int metaCount) {
        this.metaCount = metaCount;
    }

}
