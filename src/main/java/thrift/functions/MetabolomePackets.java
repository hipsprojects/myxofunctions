/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.functions;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author nibo01
 */
public class MetabolomePackets {

    public int group = 0;
    public List<Metabolome> metabolomeList = new ArrayList<Metabolome>();

    public int getGroup() {
        return group;
    }

    public void setGroup(int group) {
        this.group = group;
    }

    public List<Metabolome> getMetabolomeList() {
        return metabolomeList;
    }

    public void setMetabolomeList(List<Metabolome> metabolomeList) {
        this.metabolomeList = metabolomeList;
    }

}
