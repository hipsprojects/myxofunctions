/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thrift.main;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import thriftserver.ThriftServer;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.thrift.transport.TTransportException;
import thrift.properties.Properties;
import java.io.IOException;
import java.io.FileReader;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
/**
 *
 * @author Zhazira-pc
 */
public class MyServer {
    /*public static void StartsimpleServer(AdditionService.Processor<AdditionServiceHandler> processor) {
     try {
     TServerTransport serverTransport = new TServerSocket(8181);
     TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));
     // Use this for a multithreaded server
     // TServer server = new TThreadPoolServer(new
     // TThreadPoolServer.Args(serverTransport).processor(processor));
     System.out.println("Starting the simple server...");
     server.serve();
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/
    /*public static void StartsimpleServerwithUserList(UserListService.Processor<UserListServiceHandler> processor) {
     try {
     TServerTransport serverTransport = new TServerSocket(8181);
     TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));
     // Use this for a multithreaded server
     // TServer server = new TThreadPoolServer(new
     // TThreadPoolServer.Args(serverTransport).processor(processor));
     System.out.println("Starting the simple server...");
     server.serve();
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/

    /*public static void StartThriftServer(ThriftServerice.Processor<ThriftServiceHandler> processor) {
     try {
     TServerTransport serverTransport = new TServerSocket(8181);
     //TServer server = new TSimpleServer(new Args(serverTransport).processor(processor));
     // Use this for a multithreaded server
     TServer server = new TThreadPoolServer(new
     TThreadPoolServer.Args(serverTransport).processor(processor));
     System.out.println("Starting the simple server...");
     server.serve();
     } catch (Exception e) {
     e.printStackTrace();
     }
     }*/
    
    /**
     * connect(): Get the portNo from DB
     public static String connect() {
        String portNo = "";
        System.out.
                println("-------- MySQL JDBC Connection Testing ------------");
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("Where is your MySQL JDBC Driver?");
            e.printStackTrace();
            try {
            return portNo;
            }
            catch (Exception ex) {
                System.out.println("Problem"+e);
                ex.printStackTrace();
            }
            
        }

        Connection connection = null;
        try {
            
            if(Properties.MYSQL_SQLDATABASE_ID == 1){
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://" + Properties.MXBASE_SERVER + ":"+Properties.MYSQL_PORT_NO+"/" + Properties.MXBASE_DATABASE + "?"
                            + "user=" + Properties.DEFAULT_USER + "&password=" + Properties.DEFAULT_USER_PASSWORD + ""
                    );}
            else if(Properties.MYSQL_SQLDATABASE_ID == 2){
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://" + Properties.MXBASE_TEST_SERVER + ":"+Properties.MYSQL_PORT_NO+"/" + Properties.MXBASE_TEST_DATABASE + "?"
                            + "user=" + Properties.DEFAULT_USER + "&password=" + Properties.DEFAULT_USER_PASSWORD + ""
                    );}
            else if(Properties.MYSQL_SQLDATABASE_ID == 3){
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://" + Properties.CHEMBASE_HIPS_SERVER + ":"+Properties.MYSQL_PORT_NO+"/" + Properties.CHEMBASE_HIPS_DATABASE + "?"
                            + "user=" + Properties.DEFAULT_USER + "&password=" + Properties.DEFAULT_USER_PASSWORD + ""
                    );}           
            //Try MxBase
            else if(Properties.MYSQL_SQLDATABASE_ID == 4){
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://" + Properties.MXBASE_SERVER + ":"+Properties.MYSQL_PORT_NO+"/" + Properties.MXBASE_DATABASE + "?"
                            + "user=" + Properties.DEFAULT_USER + "&password=" + Properties.DEFAULT_USER_PASSWORD + ""
                    );}
            //Try Symrise
            else if(Properties.MYSQL_SQLDATABASE_ID == 5){
            connection = DriverManager
                    .getConnection(
                            "jdbc:mysql://" + Properties.SYMRISE_CNS_SERVER + ":" + Properties.MYSQL_PORT_NO+"/" + Properties.SYMRISE_CNS_DATABASE + "?"
                            + "user=" + Properties.DEFAULT_USER + "&password=" + Properties.DEFAULT_USER_PASSWORD + ""
                    );}
            System.out.println("Conn details"+connection);
                  
            try {
        
            Statement stmt = connection.createStatement(
                    ResultSet.TYPE_SCROLL_SENSITIVE,
                    ResultSet.CONCUR_READ_ONLY);
            
            ResultSet rs = stmt.executeQuery(
                    "select Value from Mxbase_Settings where Setting = 'Thriftservice';");
            boolean available = true;//false;
            
            try {
            String thriftService = "";
            while (rs.next()) {
                if(rs.getString("Value").equals("0")) {
                    thriftService = rs.getString("Value");
                    available = true;
                } else {
                    System.out.println("Harcoded thriftservice");
                    thriftService = "0";
                    available = true;
                    //System.out.println("ThriftServiceFromDB:"+thriftService);
                }
                
            }
            } catch (SQLException exc) {
                System.out.println("Can not read Thriftservice");
                System.out.println(exc);
                exc.printStackTrace();
            }
            
            try {
                
            if(!available)
                return portNo;
            try {
            rs = stmt.executeQuery(
                    "select Value from Mxbase_Settings where Setting = 'Thriftserviceport';");
            } catch (SQLException excep) {
                System.out.println(excep);
                excep.printStackTrace();
                System.out.println("SQLException: " + excep.getMessage());
                System.out.println("SQLState: " + excep.getSQLState());
                System.out.println("VendorError: " + excep.getErrorCode());
            }
            while (rs.next()) {
                portNo = rs.getString("Value");
                if (portNo.equals(""))
                    portNo = "8181";
                System.out.println("PortNumberFromDB:"+portNo);
            } 
            
            
            } catch (SQLException exce) {
                System.out.println("Can not read Thriftserviceport");
                System.out.println(exce);
                exce.printStackTrace();
            }
            
            }catch (SQLException ex) {
                System.out.println("Can not create a SQL statement!");
                System.out.println(ex);
                ex.printStackTrace();
                System.out.println("SQLException: " + ex.getMessage());
                System.out.println("SQLState: " + ex.getSQLState());
                System.out.println("VendorError: " + ex.getErrorCode());
            }
            
            connection.close();
            return portNo;
        } catch (SQLException e) {
            System.out.println("Connection Failed! Check output console");
            System.out.println(e);
            e.printStackTrace();
            System.out.println("SQLException: " + e.getMessage());
            System.out.println("SQLState: " + e.getSQLState());
            System.out.println("VendorError: " + e.getErrorCode());
            return portNo;
        }
    }
  */   
     
   

    public static void main(String[] args) {
        //OdbcConnection.connect();
        //StartsimpleServer(new AdditionService.Processor<AdditionServiceHandler>(new AdditionServiceHandler()));
        //StartsimpleServerwithUserList(new UserListService.Processor<UserListServiceHandler>(new UserListServiceHandler()));
        //StartThriftServer(new ThriftServerice.Processor<ThriftServiceHandler>(new ThriftServiceHandler()));
        //String portNo = connect();
        
        String fileName = "/usr/share/java/webapps/MxBaseThriftConfig.txt";
        //String fileName = "C:\\Mxbase\\Thriftservice\\MxBaseThriftConfig.txt";
        //String fileName = "C:\\Users\\nibo01\\Documents\\NetBeansProjects\\ThriftServerFunctions\\MxBaseThriftConfig.txt";
        String portNo = "";
        
        try {
            FileReader fileReader  = new FileReader(fileName);
            BufferedReader bufferedReader  = new BufferedReader(fileReader);
            portNo = bufferedReader.readLine();
            bufferedReader.close();     
        }
         catch(FileNotFoundException ex) {
            System.out.println(
                "Unable to open file '" + 
                fileName + "'");                
        }
        catch(IOException ex) {
            System.out.println(
                "Error reading file '" 
                + fileName + "'");       
        }
   
        ThriftServer server = new ThriftServer();
        try {
            server.init(portNo);
        } catch (InterruptedException ex) {
            Logger.getLogger(MyServer.class.getName()).
                    log(Level.SEVERE,
                            null,
                            ex);
            System.out.println("InterruptedException"+ex);
        } catch (TTransportException ex) {
            Logger.getLogger(MyServer.class.getName()).
                    log(Level.SEVERE,
                            null,
                            ex);
            System.out.println("TTransportException"+ex);
        }
    }
}
