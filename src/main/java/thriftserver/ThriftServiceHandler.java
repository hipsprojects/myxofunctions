/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package thriftserver;

import java.io.UnsupportedEncodingException;
import thrift.functions.ServerFunctionImplementations;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.thrift.TException;

/**
 * This class has all Thrift Server-side functions implementation which can be
 * called from Client-side.
 *
 * @author Zhazira-pc
 */
public class ThriftServiceHandler
        implements ThriftService.Iface {

    /**
     * Initialy has null value, gets value after mySQLConnect method is called.
     */
    //public Connection connection = null;
    public String mainDatabase = "";

    /**
     *
     */
    public String mainServer = "";

    /**
     *
     */
    public String mainUsername = "";

    /**
     *
     */
    public final int serverTimes = 10;

    /**
     * Test call to check whether there is connection to the thrift.
     *
     * @param test
     * @return failure, success message.
     */
    public String testConnection(boolean test) {
        if (test) {
            return "Success";
        }
        return "Problem";
    }

    /**
     * Connects to the MySQL database server with the default Thrift_User.
     *
     * @param server server name
     * @param database database name
     * @param userName omit
     * @param userpw omit
     * @return failure, success message.
     * @throws org.apache.thrift.TException
     */
    public String mySQLConnect(String server,
            String database,
            String userName,
            String userpw)
            throws org.apache.thrift.TException {

        mainDatabase = database;
        mainServer = server;
        mainUsername = userName;

        System.out.println("ThriftServiceHandler.mySQLConnect(" + server + "," + database + "," + userName + ")");
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        String result = sfi.testMySQLConnection(server,
                database, userName);
        //getUserList();
        return result;
    }

    /**
     * Returns obtained TA Screening export results for each compound key in a
     * given c_ids list.
     *
     * @param headerLine include header line.
     * @param withoutAnalData include compounds without Analytical data.
     * @param retentionTime include compounds without Retention time.
     * @param pseuFormula generate neutral pseudo-formulae.
     * @param analDetailsToName append analytical details to names.
     * @param polarity the polarity value.
     * @param analSystem the Analytical System value.
     * @param c_ids the list of selected compound keys.
     * @return list of all export result lines.
     * @throws org.apache.thrift.TException
     */
    public List<String> bruckerTAScreening_Export(boolean headerLine,
            boolean withoutAnalData,
            boolean retentionTime,
            boolean pseuFormula,
            boolean analDetailsToName,
            String polarity,
            String analSystem,
            List<Integer> c_ids)
            throws org.apache.thrift.TException {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }

        List<String> resultStr = new ArrayList<String>();

        if (connection != null) {
            List<String> newString = sfi.BruckerTAScrrening(headerLine,
                    withoutAnalData,
                    retentionTime,
                    pseuFormula,
                    analDetailsToName,
                    polarity,
                    analSystem,
                    c_ids,
                    connection);
            try {
                for (String str : newString) {
                    String result = new String(str.getBytes("UTF-8"), "UTF-8");
                    resultStr.add(result);
                }
                //return newString;
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ThriftServiceHandler.class.getName()).
                        log(Level.SEVERE,
                                null,
                                ex);
            }
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultStr;
    }

    /**
     * Reads XML files from the S_Metabolites_TAFiles table for each SCRN_Key in
     * a given SRCNList, parses each XML file and writes obtained TS_Screen
     * results into the S_Metabolites_TAresults table with its corresponding
     * SCRN_Key.
     *
     * @param SCRNList the list of SCRN_Keys.
     * @param TAScore the minimum TA Score value (has value "Disabled" if this
     * filter option is ignored).
     * @param area the minimum area value (has value "Disabled" if this filter
     * option is ignored).
     * @param intensity the minimum intensity value (has value "Disabled" if
     * this filter option is ignored)
     * @param massDev the mass deviation threshold (has value "Disabled" if this
     * filter option is ignored).
     * @param mSigma the mSigma threshold (has value "Disabled" if this filter
     * option is ignored).
     * @return failure, success message.
     */
    public String taSreeningXMLParser(List<String> SCRNList, String TAScore, String area, String intensity, String massDev, String mSigma) {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }

        String result = "";
        if (connection != null) {
            //ServerFunctionImplementations sfi = new ServerFunctionImplementations();
            result = sfi.taSreeningXMLParser(SCRNList, TAScore, area, intensity, massDev, mSigma, connection);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Calculates intensities and masses for a given ion Formula with ion charge
     * using IPC libraries.
     *
     * @param ionFormula the ion formula.
     * @param ionCharge the ion charge (has value "empty" if ion charge is
     * ignored).
     * @return the list of intensities and masses.
     * @throws org.apache.thrift.TException
     */
    public List<String> ipc(String ionFormula, String ionCharge) throws TException {

        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }
        List<String> result = new ArrayList<String>();
        if (connection != null) {
            result = sfi.ipc(ionFormula, ionCharge);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Reads XML files from the S_Metabolome_blobs table for each META_Key in a
     * given METAList, parses each XML file and writes obtained meta results
     * into the S_Metabolite_mining table with its corresponding META_Key.
     *
     * @param METAList the list of META_Keys.
     * @param intensity the minimum intensity value (has value "Disabled" if
     * this filter option is ignored).
     * @param area the minimum area value (has value "Disabled" if this filter
     * option is ignored).
     * @param peaksNo the no of min.peaks in spectrum (has value "Disabled" if
     * this filter option is ignored).
     * @param retTime the retention time threshold (has value "Disabled" if this
     * filter option is ignored).
     * @param recalculateCharge recalculate charge (false if this filter options
     * is ignored)
     * @return an empty string.
     */
    public String metabolomeMiningXMLParser(List<String> METAList, String intensity, String area, String peaksNo, String retTime, boolean recalculateCharge) {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }
        String result = "";
        if (connection != null) {
            //ServerFunctionImplementations sfi = new ServerFunctionImplementations();
            result = sfi.metabolomeMiningXMLParser(METAList, intensity, area, peaksNo, retTime, recalculateCharge, connection);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return result;
    }

    /**
     * Reads query parameters, query ID, query type from the QRY_Experiments
     * table for each QRY_Key in a given QRY_IDList, make MySQL queries, and
     * write results into the QRY_Results table with its corresponding query ID
     * and query type.
     *
     * @param QRY_IDList list of QRY_Keys.
     */
    public void metabolomeMiningQuery(List<String> QRY_IDList) {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }

        if (connection != null) {
            //ServerFunctionImplementations sfi = new ServerFunctionImplementations();
            sfi.metabolomeMiningQuery(QRY_IDList, connection);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Gets polarity, ion_list and of c_ids and calculates the neutral mass,
     * ionized formula and ionized mass.
     *
     * @param polarity
     * @param ion_list
     * @param c_ids
     * @return list of all generated ion list.
     * @throws TException
     */
    public List<String> generateIonList(String polarity, List<String> ion_list, List<Integer> c_ids) throws org.apache.thrift.TException {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }

        List<String> resultStr = new ArrayList<String>();

        if (connection != null) {
            List<String> newString = sfi.generateIonList(polarity, ion_list, c_ids, connection);
            try {
                for (String str : newString) {
                    String result = new String(str.getBytes("UTF-8"), "UTF-8");
                    resultStr.add(result);
                }
            } catch (UnsupportedEncodingException ex) {
                Logger.getLogger(ThriftServiceHandler.class.getName()).
                        log(Level.SEVERE,
                                null,
                                ex);
            }
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return resultStr;
    }

    /**
     * 
     * @return name of the jar file name and the version number.
     */
    public String versionProvider() {

        String result = "";
        String version = "";
        String name = "";

        ResourceBundle rbversion;
        ResourceBundle rbartifactId;
        try {
            rbartifactId = ResourceBundle.getBundle("primefaces-extensions");
            name = rbartifactId.getString("application.artifactId");

            rbversion = ResourceBundle.getBundle("primefaces-extensions");
            version = rbversion.getString("application.version");
            result = name + "-" + version + ".jar";

        } catch (MissingResourceException e) {
            System.out.println("Resource bundle 'primefaces-extensions' was not found or error while reading current version.");
        }

        return result;
    }

    /**
     * Filters out and sorts the META features in the main table by comparing with the matrix table.
     * 
     * @param massRange
     * @param retTimeRange
     * @param mainTable
     * @param matrixTable
     * @param operationId
     * @param metaKeyList
     */
    public void matrixFilter(double massRange, double retTimeRange, String mainTable, String matrixTable, int operationId, String metaKeyList) {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }

        if (connection != null) {
            String[] splitName = mainUsername.split("_");
            String name = splitName[0];
            sfi.matrix(name, massRange, retTimeRange, mainTable, matrixTable, operationId, metaKeyList, connection);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Filters out the META features in the table depending on the threshold values.
     * 
     * @param mzMin
     * @param mzMax
     * @param retTimeMin
     * @param retTimeMax
     * @param intenMin
     * @param intenMax
     * @param sn
     * @param peakNr
     * @param mArea
     * @param tableName
     * @param minRKey
     * @param maxRKey
     * @param operationId
     */
    public void thresholdFilter(double mzMin, double mzMax, double retTimeMin, double retTimeMax, double intenMin, double intenMax, String sn, int peakNr, double mArea, String tableName, int minRKey, int maxRKey, int operationId) {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }
        if (connection != null) {
            String[] splitName = mainUsername.split("_");
            String name = splitName[0];
            sfi.threshold(name, mzMin, mzMax, retTimeMin, retTimeMax, intenMin, intenMax, sn, peakNr, mArea, tableName, minRKey, maxRKey, operationId, connection);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Buckets META features in the chosen table depending on the preset range.
     *  
     * @param minRKey
     * @param maxRKey
     * @param massRange
     * @param retTimeRange
     * @param tableName
     * @param operationId
     */
    public void metabolomeCombiner(int minRKey, int maxRKey, double massRange, double retTimeRange, String tableName, int operationId) {

        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;
        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }
        if (connection != null) {
            String[] splitName = mainUsername.split("_");
            String name = splitName[0];
            sfi.metabolomeCombinerQuery(name, minRKey, maxRKey, massRange, retTimeRange, tableName, operationId, connection);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     *Creates Distribution Matrix in order to create Distance Matrix, Hierarchical Clustering and Heatmap.
     * 
     * @param operationId
     * @param cmbBoxDistributionPhylLevel
     * @return Distribution Matrix
     */
        public List<List<String>> generateCSVFile(int operationId, int cmbBoxDistributionPhylLevel) {
        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;

        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }
        List<List<String>> csvDetails = new ArrayList<List<String>>();

        if (connection != null) {

            csvDetails = sfi.generateCSV(operationId, cmbBoxDistributionPhylLevel, connection);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }

        return csvDetails;
    }

    /**
     * Basic test to check the client/server side function.
     * @param test
     * @return success message.
     */
    public String testMe(String test) {

        Connection connection = null;
        ServerFunctionImplementations sfi = new ServerFunctionImplementations();
        int i = 0;

        while (connection == null && i < serverTimes) {
            connection = sfi.getMySQLConnection(mainServer,
                    mainDatabase,
                    mainUsername,
                    "");
            i++;
        }
        if (connection != null) {
            System.out.println("answer " + test);
        }
        try {
            connection.close();
        } catch (SQLException ex) {
            Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "success";
    }
    /*
   
     public List<String> metabolomeAnnotator(String id, String retentionTimeMin, String retentionTimeMax, String key) {
     Connection connection = null;   
     ServerFunctionImplementations sfi = new ServerFunctionImplementations();
     int i=0;
     while(connection == null && i < serverTimes){
     connection = sfi.getMySQLConnection(mainServer,
     mainDatabase,
     mainUsername,
     "");
     i++;
     }
                
     List<String> queryResult = new ArrayList<String>();
        
        
        
     if (connection != null) {
     List<String> query = sfi.metabolomeAnnotatorQuery(id, retentionTimeMin, retentionTimeMax, key, connection);
     try {
     for (String str : query) {
     System.out.println();
     String result = new String(str.getBytes("UTF-8"), "UTF-8");
     queryResult.add(result);
     }
     //return newString;
     } catch (UnsupportedEncodingException ex) {
     Logger.getLogger(ThriftServiceHandler.class.getName()).
     log(Level.SEVERE,
     null,
     ex);
     }
     }
     try {
     connection.close();
     } catch (SQLException ex) {
     Logger.getLogger(ThriftServiceHandler.class.getName()).log(Level.SEVERE, null, ex);
     }
        
        
        
     return queryResult;
     }

     */
}
